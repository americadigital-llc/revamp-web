<?php
/**
 * keystroke functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package keystroke
 */

define('AXIL_THEME_URI', get_template_directory_uri());
define('AXIL_THEME_DIR', get_template_directory());
define('AXIL_CSS_URL', get_template_directory_uri() . '/assets/css/');
define('AXIL_JS_URL', get_template_directory_uri() . '/assets/js/');
define('AXIL_ADMIN_CSS_URL', get_template_directory_uri() . '/assets/admin/css/');
define('AXIL_ADMIN_JS_URL', get_template_directory_uri() . '/assets/admin/js/');
define('AXIL_FREAMWORK_DIRECTORY', AXIL_THEME_DIR . '/inc/');
define('AXIL_FREAMWORK_HELPER', AXIL_THEME_DIR . '/inc/helper/');
define('AXIL_FREAMWORK_OPTIONS', AXIL_THEME_DIR . '/inc/options/');
define('AXIL_FREAMWORK_CUSTOMIZER', AXIL_THEME_DIR . '/inc/customizer/');
define('AXIL_THEME_FIX', 'axil');
define('AXIL_FREAMWORK_LAB', AXIL_THEME_DIR . '/inc/lab/');
define('AXIL_FREAMWORK_TP', AXIL_THEME_DIR . '/template-parts/');
define('AXIL_IMG_URL', AXIL_THEME_URI . '/assets/images/logo/');


$axi_theme_data = wp_get_theme();
define('AXIL_VERSION', (WP_DEBUG) ? time() : $axi_theme_data->get('Version'));


if (!function_exists('keystroke_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function keystroke_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on keystroke, use a find and replace
         * to change 'keystroke' to the name of your theme in all the template files.
         */
        load_theme_textdomain('keystroke', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'keystroke'),
            'offcanvas' => esc_html__('Off Canvas', 'keystroke'),
            'footerbottom' => esc_html__('Footer Bottom Menu (No depth supported)', 'keystroke'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Post Format
         */
        add_theme_support('post-formats', array('gallery', 'link', 'quote', 'video', 'audio'));


        add_editor_style( array( 'style-editor.css', axil_fonts_url() ) );
        add_theme_support('responsive-embeds');
        add_theme_support('wp-block-styles');
        add_theme_support('editor-styles');
        add_editor_style('style-editor.css');

        // for gutenberg support
        add_theme_support( 'align-wide' );
        add_theme_support( 'editor-color-palette', array(
            array(
                'name' => esc_html__( 'Primary', 'keystroke' ),
                'slug' => 'primary',
                'color' => '#702FFF',
            ),
            array(
                'name' => esc_html__( 'Secondary', 'keystroke' ),
                'slug' => 'secondary',
                'color' => '#00D09C',
            ),
            array(
                'name' => esc_html__( 'Tertiary', 'keystroke' ),
                'slug' => 'tertiary',
                'color' => '#FFCD3E',
            ),
            array(
                'name' => esc_html__( 'White', 'keystroke' ),
                'slug' => 'white',
                'color' => '#ffffff',
            ),
            array(
                'name' => esc_html__( 'Dark Light', 'keystroke' ),
                'slug' => 'dark-light',
                'color' => '#6B7074',
            ),
        ) );

        add_theme_support( 'editor-font-sizes', array(
            array(
                'name' => esc_html__( 'Small', 'keystroke' ),
                'size' => 12,
                'slug' => 'small'
            ),
            array(
                'name' => esc_html__( 'Normal', 'keystroke' ),
                'size' => 16,
                'slug' => 'normal'
            ),
            array(
                'name' => esc_html__( 'Large', 'keystroke' ),
                'size' => 36,
                'slug' => 'large'
            ),
            array(
                'name' => esc_html__( 'Huge', 'keystroke' ),
                'size' => 50,
                'slug' => 'huge'
            )
        ) );

        /**
         * Add Custom Image Size
         */
        add_image_size('axil-blog-thumb', 813, 431, true);
        add_image_size('axil-single-blog-thumb', 1260, 500, true);
        add_image_size('axil-casestudy-thumb', 668, 661, true);
        add_image_size('axil-casestudy-project-thumb', 600, 700, true);
        add_image_size('axil-project-thumb', 400, 380, true);
        add_image_size('axil-latest-story-thumb', 410, 452, true);
        add_image_size('axil-team-thumb', 630, 630, true);
        add_image_size('axil-project-banner-thumb', 368, 480, true);
    }
endif;
add_action('after_setup_theme', 'keystroke_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function keystroke_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('keystroke_content_width', 640);
}

add_action('after_setup_theme', 'keystroke_content_width', 0);


/**
 * Enqueue scripts and styles.
 */
require_once(AXIL_FREAMWORK_DIRECTORY . "scripts.php");
/**
 * Global Functions
 */
require_once(AXIL_FREAMWORK_DIRECTORY . "global-functions.php");

/**
 * Register Custom Widget Area
 */
require_once(AXIL_FREAMWORK_DIRECTORY . "widget-area-register.php");

/**
 * Register Custom Fonts
 */
require_once(AXIL_FREAMWORK_DIRECTORY . "register-custom-fonts.php");

/**
 * TGM
 */
require_once(AXIL_FREAMWORK_DIRECTORY . "tgm-config.php");


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/underscore/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/underscore/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/underscore/template-functions.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/underscore/jetpack.php';
}

/**
 * Helper Template
 */
require_once(AXIL_FREAMWORK_HELPER . "menu-area-trait.php");
require_once(AXIL_FREAMWORK_HELPER . "layout-trait.php");
require_once(AXIL_FREAMWORK_HELPER . "option-trait.php");
require_once(AXIL_FREAMWORK_HELPER . "meta-trait.php");
require_once(AXIL_FREAMWORK_HELPER . "banner-trait.php");
// Helper
require_once(AXIL_FREAMWORK_HELPER . "helper.php");

/**
 * Options
 */
require_once(AXIL_FREAMWORK_OPTIONS . "theme/option-framework.php");
require_once(AXIL_FREAMWORK_OPTIONS . "page-options.php");
require_once(AXIL_FREAMWORK_OPTIONS . "post-format-options.php");
require_once(AXIL_FREAMWORK_OPTIONS . "user-extra-meta.php");
require_once(AXIL_FREAMWORK_OPTIONS . "team-extra-meta.php");

/**
 * Customizer
 */
require_once(AXIL_FREAMWORK_CUSTOMIZER . "color.php");

/**
 * Lab
 */
require_once(AXIL_FREAMWORK_LAB . "class-tgm-plugin-activation.php");
// -- Nav Walker
require_once(AXIL_FREAMWORK_LAB . "aw-nav-menu-walker.php");
require_once(AXIL_FREAMWORK_LAB . "aw-mobile-menu-walker.php");
require_once(AXIL_FREAMWORK_TP . "title/breadcrumb.php");



