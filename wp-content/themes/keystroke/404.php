<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package keystroke
 */

get_header();
// Get Value
$axil_options = Helper::axil_get_options();

?>

<!-- Start Page Error Area  -->
<div class="axil-error-not-found fullscreen d-flex align-items-center theme-gradient-7 list-active">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="axil-error text-center">
					<div class="inner">
					<?php if(!empty($axil_options['axil_404_title'])){ ?> <h1 class="title mb--20"><?php echo esc_html( $axil_options['axil_404_title'] );?></h1> <?php } ?>
					<?php if(!empty($axil_options['axil_404_subtitle'])){ ?> <p class="subtitle-2 mb--30"><?php echo esc_html( $axil_options['axil_404_subtitle'] );?></p> <?php } ?>
						<?php if( $axil_options['axil_enable_go_back_btn'] !== "no"){ ?> 
							<a href="<?php echo esc_url( home_url( '/' ) );?>" class="axil-button btn-large btn-transparent"><span class="button-text"><?php echo esc_html( $axil_options['axil_button_text'] );?></span><span class="button-icon"></span></a>
						<?php } ?>

						<img class="text-image" src="<?php echo get_template_directory_uri(); ?>/assets/images/others/404.png" alt="404 Images">
							
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="shape-group">
		<div class="shape shape-01">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/shape/404-01.png" alt="Shape Images">
		</div>
		<div class="shape shape-02">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/shape/404-01.png" alt="Shape Images">
		</div>
		<div class="shape shape-03">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/shape/404-02.png" alt="Shape Images">
		</div>
		<div class="shape shape-04">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/images/shape/404-03.png" alt="Shape Images">
		</div>
	</div>
</div>
<!-- End Page Error Area  -->

<?php
get_footer();
