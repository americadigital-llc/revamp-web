<?php
/**
 * Template part for displaying footer layout two
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

// Get Value
$axil_options = Helper::axil_get_options();
$axil_footer_bottom_menu_args = Helper::axil_footer_bottom_menu_args();
?>
<footer class="axil-footer-area bg-color-lightest axil-footer-style-4">

    <!-- Start Copyright -->
    <div class="copyright copyright-default">
        <div class="container">
            <div class="row row--0 ptb--20 axil-basic-thine-line align-items-center">
                <?php if(!empty($axil_options['axil_copyright_contact'])){ ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="inner text-center text-md-left">
                            <p><?php echo wp_kses_post($axil_options['axil_copyright_contact']); ?></p>
                        </div>
                    </div>
                <?php } ?>
                <?php if (has_nav_menu('footerbottom')) { ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php wp_nav_menu($axil_footer_bottom_menu_args); ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- End Copyright -->
</footer>
