<?php
/**
 * Template part for displaying footer layout one
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

// Get Value
$axil_options = Helper::axil_get_options();
$axil_footer_bottom_menu_args = Helper::axil_footer_bottom_menu_args();
?>
<!-- Start Footer Area -->
<footer class="axil-footer footer-default theme-gradient-2 axil-footer-style-1">
    <div class="bg_image--2">
        <?php if(!empty($axil_options['axil_social_icons']) && !empty($axil_options['axil_footer_social_network'])){ ?>
            <!-- Start Social Icon -->
            <div class="ft-social-icon-wrapper ax-section-gapTop">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="ft-social-share d-flex justify-content-center liststyle flex-wrap">
                                <?php
                                foreach ($axil_options['axil_social_icons'] as $key => $value) {
                                    if ($value != '') {
                                        echo '<li><a class="' . esc_attr($key) . ' social-icon" href="' . esc_url($value) . '" title="' . esc_attr(ucwords(esc_attr($key))) . '" target="_blank"><i class="fab fa-' . esc_attr($key) . '"></i></a></li>';
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Social Icon -->
        <?php } ?>

        <?php if(is_active_sidebar('footer-1') || is_active_sidebar('footer-2') || is_active_sidebar('footer-3') || is_active_sidebar('footer-4') ){ ?>
            <!-- Start Footer Top Area -->
            <div class="footer-top ax-section-gap">
                <div class="container">
                    <div class="row">

                        <?php if (is_active_sidebar('footer-1')) { ?>
                            <!-- Start Single Widget -->
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="footer-widget-item axil-border-right">
                                    <?php dynamic_sidebar('footer-1'); ?>
                                </div>
                            </div><!-- End Single Widget -->
                        <?php } ?>

                        <?php if (is_active_sidebar('footer-2')) { ?>
                            <!-- Start Single Widget -->
                            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mt_mobile--5">
                                <div class="footer-widget-item">
                                    <?php dynamic_sidebar('footer-2'); ?>
                                </div>
                            </div><!-- End Single Widget -->
                        <?php } ?>

                        <?php if (is_active_sidebar('footer-3')) { ?>
                            <!-- Start Single Widget -->
                            <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 mt_lg--5 mt_md--5 mt_sm--5">
                                <div class="footer-widget-item">
                                    <?php dynamic_sidebar('footer-3'); ?>
                                </div>
                            </div><!-- End Single Widget -->
                        <?php } ?>

                        <?php if (is_active_sidebar('footer-4')) { ?>
                            <!-- Start Single Widget -->
                            <div class="col-xl-1 col-lg-6 col-md-6 col-sm-6 col-12 mt_lg--5 mt_md--5 mt_sm--5">
                                <div class="footer-widget-item widget-last">
                                    <?php dynamic_sidebar('footer-4'); ?>
                                </div>
                            </div><!-- End Single Widget -->
                        <?php } ?>

                    </div>
                </div>
            </div>
            <!-- End Footer Top Area -->
        <?php } ?>
        <!-- Start Copyright -->
        <div class="copyright copyright-default">
            <div class="container">
                <div class="row row--0 ptb--20 axil-basic-thine-line align-items-center">
                    <?php if(!empty($axil_options['axil_copyright_contact'])){ ?>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="inner text-center text-md-left">
                                <p><?php echo wp_kses_post($axil_options['axil_copyright_contact']); ?></p>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (has_nav_menu('footerbottom')) { ?>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <?php wp_nav_menu($axil_footer_bottom_menu_args); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>

    </div>
    <!-- End Copyright -->
</footer>
<!-- End Footer Area -->
