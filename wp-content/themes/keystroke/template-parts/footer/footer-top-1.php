<?php
/**
 * Template part for displaying footer top layout one
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

// Get Value
$axil_options = Helper::axil_get_options();
// Get data from page option
$op_footer_top_area        = axil_get_acf_data('axil_show_footer_top');
// Get data from theme option
$ot_footer_top_area         = $axil_options['axil_footer_top_enable'];
if (empty($op_footer_top_area)){
    $before_title            = $axil_options['axil_ft_before_title'];
    $title                   = $axil_options['axil_ft_title'];
    $button_text             = $axil_options['axil_ft_button_text'];
    $link_type               = $axil_options['axil_ft_button_link_type'];
    $custom_link             = $axil_options['axil_ft_button_link'];
    $page_link               = get_permalink($axil_options['axil_ft_button_page_link']);
    $button_below_content    = $axil_options['axil_ft_button_below_content'];
} else {
    $before_title           = axil_get_acf_data("axil_footer_top_before_title");
    $title                  = axil_get_acf_data("axil_footer_top_title");
    $button_text            = axil_get_acf_data("axil_footer_top_button_text");
    $link_type              = axil_get_acf_data("axil_footer_top_select_link_type");
    $custom_link            = axil_get_acf_data("axil_footer_top_button_custom_link");
    $page_link              = axil_get_acf_data("select_footer_top_button_page_link");
    $button_below_content   = axil_get_acf_data("axil_footer_top_button_below_content");
}

// Link
if ('2' == $link_type) {
    $button_url = $page_link;
} else {
    $button_url = $custom_link;
}

?>

<!-- Start Call To Action -->
<div class="axil-call-to-action-area shape-position ax-section-gap theme-gradient">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="axil-call-to-action">
                    <div class="section-title text-center">
                        <?php if (!empty($before_title)){ ?>
                            <span class="sub-title extra04-color"><?php echo esc_html($before_title); ?></span>
                        <?php } ?>
                        <?php if (!empty($title)){ ?>
                            <h2 class="title"><?php echo esc_html($title); ?></h2>
                        <?php } ?>
                        <?php if (!empty($button_text)){ ?>
                            <a class="axil-button wow slideFadeInUp  btn-transparent btn-large" href="<?php echo esc_url($button_url); ?>"><span class="button-text"><?php echo esc_html($button_text); ?></span><span class="button-icon"></span></a>
                        <?php } ?>
                        <?php if (!empty($button_below_content)){ ?>
                            <div class="callto-action">
                                <?php echo awescapeing($button_below_content); ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="shape-group">
        <div class="shape shape-01">
            <span class="icon icon-shape-14"></span>
        </div>
        <div class="shape shape-02">
            <span class="icon icon-breadcrumb-1"></span>
        </div>
        <div class="shape shape-03">
            <span class="icon icon-shape-10"></span>
        </div>
        <div class="shape shape-04">
            <span class="icon icon-breadcrumb-2"></span>
        </div>
    </div>
</div>
<!-- End Call To Action -->