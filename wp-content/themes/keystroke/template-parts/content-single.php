<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */
$images = axil_get_acf_data('axil_gallery_image');
$audio_url = axil_get_acf_data('axil_upload_audio');
$custom_link = axil_get_acf_data('axil_custom_link');

$link = !empty($custom_link) ? $custom_link : get_the_permalink();
$axil_quote_author_name = axil_get_acf_data("axil_quote_author_name");
$axil_quote_author = !empty($axil_quote_author_name) ? $axil_quote_author_name : get_the_author();
$axil_quote_author_name_designation = axil_get_acf_data("axil_quote_author_name_designation");
$video_url = axil_get_acf_data("axil_video_link");
$axil_options = Helper::axil_get_options();

$thumb_size = ($axil_options['axil_single_pos'] === 'full') ? 'axil-single-blog-thumb':'axil-blog-thumb';

?>
<article id="post-<?php the_ID(); ?>" <?php post_class('wrapper single-content'); ?>>

    <?php if (!has_post_format('quote') && !has_post_format('link')) { ?>
        <div class="blog-top">
            <h1 class="title"><?php the_title(); ?></h1>
            <?php Helper::axil_singlepostmeta(); ?>
        </div>
    <?php } ?>

    <?php
    if (has_post_format('gallery')) {
        if ($images): ?>
            <div class="thumbnail axil-gallery-post axil-slick-dotstyle axil-arrow-button botton-bottom-transparent">
                <?php foreach( $images as $image ): ?>
                    <div class="thumb-inner">
                        <img class="w-100"  src="<?php echo esc_url($image['sizes'][$thumb_size]); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif;
    } else if (has_post_format('audio')) {
        ?>
        <div class="mb--60"><?php
        if (has_post_thumbnail()) { ?>
            <div class="thumbnail position-relative">
                <?php the_post_thumbnail($thumb_size, ['class' => 'w-100']) ?>
            </div>
        <?php } ?>

        <?php if ($audio_url): ?>
            <audio controls>
                <source src="<?php echo esc_url($audio_url['url']); ?>" type="audio/ogg">
                <source src="<?php echo esc_url($audio_url['url']); ?>" type="audio/mpeg">
                <?php esc_html_e('Your browser does not support the audio tag.', 'keystroke'); ?>
            </audio>
        <?php endif;
        ?></div><?php
    } else if (has_post_format('link')) {
        ?>
        <div class="mb--60">
            <div id="post-<?php the_ID(); ?>" <?php post_class('mb--20 axil-blog-list sticky-blog mt_md--30 mt_sm--30 mt_lg--50'); ?>>
                <div class="blog-top">
                    <h1 class="title"><a href="<?php echo esc_url($link); ?>"><?php the_title(); ?></a></h1>
                    <div class="info">
                        <div class="info d-flex align-items-center">
                            <h6 class="mb--0 pr--10"><?php the_author(); ?></h6>
                            <ul class="blog-meta d-flex align-items-center liststyle">
                                <li><?php echo get_the_time(get_option('date_format')); ?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="sticky">
                        <i class="fas fa-link"></i>
                    </div>
                    <div class="shape-group">
                        <div class="shape shape-1">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/others/shape-17.svg"
                                 alt="Shape Image">
                        </div>
                        <div class="shape shape-2">
                            <?php echo get_template_part('assets/images/others/shape-18'); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="blog-top">
                <div class="author">
                    <div class="author-thumb">
                        <?php echo get_avatar(get_the_author_meta('ID'), 50); ?>
                    </div>
                    <div class="info">
                        <h6><?php the_author(); ?></h6>
                        <ul class="blog-meta">
                            <li><?php echo get_the_time(get_option('date_format')); ?></li>
                            <li><?php echo keystroke_content_estimated_reading_time(get_the_content()); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php
    } else if (has_post_format('quote')) {
        ?>
        <div class="mb--60">
            <!-- Start Single Blog  -->
            <div <?php post_class('mb--20 axil-blog-list quote-blog  mt_md--30 mt_sm--30 mt_lg--50'); ?>>
                <div class="blog-top">
                    <blockquote>
                        <h1 class="title"><?php the_title(); ?></h1>
                    </blockquote>
                    <div class="author">
                        <div class="info">
                            <h6><?php echo esc_html($axil_quote_author); ?></h6>
                            <ul class="blog-meta">
                                <li><?php echo esc_html($axil_quote_author_name_designation); ?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="shape-group">
                        <div class="shape shape-1">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/others/shape-17.svg"
                                 alt="Shape Image">
                        </div>
                        <div class="shape shape-2">
                            <?php echo get_template_part('assets/images/others/shape-18'); ?>
                        </div>
                        <div class="shape shape-3">
                            <?php echo get_template_part('assets/images/others/quote'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="blog-top">
                <div class="author">
                    <div class="author-thumb">
                        <?php echo get_avatar(get_the_author_meta('ID'), 50); ?>
                    </div>
                    <div class="info">
                        <h6><?php the_author(); ?></h6>
                        <ul class="blog-meta">
                            <li><?php echo get_the_time(get_option('date_format')); ?></li>
                            <li><?php echo keystroke_content_estimated_reading_time(get_the_content()); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Single Blog  -->
        <?php
    } else if (has_post_format('video')) {
        if (has_post_thumbnail()) { ?>
            <div class="thumbnail mb--60 position-relative">
                <?php the_post_thumbnail($thumb_size, ['class' => 'w-100']) ?>
                <?php if (!empty($video_url)) { ?>
                    <div class="video-button position-to-top">
                        <a class="play__btn video-btn" href="<?php echo esc_url($video_url); ?>"><span
                                    class="triangle"></span></a>
                    </div>
                <?php } ?>

            </div>
        <?php }
    } else {
        if (has_post_thumbnail()) { ?>
            <div class="thumbnail mb--60">
                <?php the_post_thumbnail($thumb_size) ?>
            </div>
        <?php }
    }
    ?>

    <?php the_content(); ?>

    <?php wp_link_pages( array(
        'before'      => '<div class="axil-page-links"><span class="page-link-holder">' . esc_html__( 'Pages:', 'keystroke' ) . '</span>',
        'after'       => '</div>',
        'link_before' => '<span>',
        'link_after'  => '</span>',
    ) );
    ?>

</article>

<?php
    if (function_exists('axil_sharing_icon_links') && $axil_options['axil_blog_details_social_share']) {
        axil_sharing_icon_links();
    }
    ?>
    <!-- Start Blog Author  -->
    <?php
    if ($axil_options['axil_blog_details_show_author_info']) {
        get_template_part('template-parts/biography');
    }
    ?>
    <!-- End Blog Author  -->
    <?php

    /**
     *  Output comments wrapper if it's a post, or if comments are open,
     * or if there's a comment number – and check for password.
     * */
    if ((is_single() || is_page()) && (comments_open() || get_comments_number()) && !post_password_required()) {
        ?>

        <div class="comments-wrapper section-inner">

            <?php comments_template(); ?>

        </div><!-- .comments-wrapper -->

        <?php
    }

?>

