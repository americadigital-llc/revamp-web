<?php
$axil_options = Helper::axil_get_options();
$axil_offcanvas_menu_args = Helper::axil_offcanvas_menu_args();


$phoneNumber = preg_replace("/[^0-9+]/", '', $axil_options['phone']);
$faxNumber = preg_replace("/[^0-9+]/", '', $axil_options['fax']);

$header_layout = Helper::axil_header_layout();
$sidenav_class = ($header_layout['header_style'] == '2') ? 'side-nav__left' : '';
?>

<!-- Start Sidebar Area  -->
<div class="side-nav">
    <div class="side-nab-top">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                <div class="logo">
                    <?php if (isset($axil_options['axil_logo_type'])) : ?>
                        <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name')); ?>" rel="home">

                            <?php if ('image' == $axil_options['axil_logo_type']) : ?>

                                <img src="<?php echo esc_url($axil_options['axil_head_logo_white']['url']); ?>" alt="<?php echo esc_attr(get_bloginfo('name')); ?>">

                            <?php else : ?>

                                <?php if ('text' == $axil_options['axil_logo_type']) : ?>

                                    <?php echo esc_html($axil_options['axil_logo_text']); ?>

                                <?php endif ?>

                            <?php endif ?>

                        </a>
                    <?php else : ?>

                        <h3>
                            <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
                                <?php if (isset($axil_options['axil_logo_text']) ? $axil_options['axil_logo_text'] : '') {
                                    echo esc_html($axil_options['axil_logo_text']);
                                } else {
                                    bloginfo('name');
                                }
                                ?>
                            </a>
                        </h3>

                        <?php $description = get_bloginfo('description', 'display');

                        if ($description || is_customize_preview()) { ?>

                            <p class="site-description"><?php echo esc_html($description); ?> </p>

                        <?php } ?>

                    <?php endif ?>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                <div class="sidebar-top-right d-flex align-items-center justify-content-end">
                <?php if (!empty($axil_options['axil_social_icons'])) { ?>
                    <div class="sidebar-share-icons">
                        <div class="contact-social-share d-flex align-items-center">
                            <ul class="social-share">
                                <?php
                                foreach ($axil_options['axil_social_icons'] as $key => $value) {
                                    if ($value != '') {
                                        echo '<li><a class="' . esc_attr($key) . ' social-icon" href="' . esc_url($value) . '"  target="_blank"><i class="fab fa-' . esc_attr($key) . '"></i></a></li>';
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <!-- End of .side-nav-inner -->
                    <?php } ?>
                    <div class="close-sidenav" id="close-sidenav">
                        <button class="close-button"><i class="fal fa-times"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="side-nav-inner">
        <!-- Start Side Content  -->
        <div class="side-nav-content">
            <div class="row ">
                <?php if (has_nav_menu('offcanvas')) { ?>
                    <!-- Start Left Bar  -->
                    <div class="col-lg-7 col-xl-8 col-12">
                        <?php wp_nav_menu($axil_offcanvas_menu_args); ?>
                    </div>
                    <!-- End Left Bar  -->
                <?php } ?>
                <!-- Start Right Bar  -->
                <div class="col-lg-5 col-xl-4 col-12">
                    <div class="axil-contact-info-inner">

                        <!-- Start Single Address  -->
                        <div class="axil-contact-info">
                            
                            <address class="address">
                                <?php
                                if ($axil_options['call_now_field_title']) { ?>
                                    <span class="title"> <?php echo wp_kses_post($axil_options['call_now_field_title']); ?> </span>
                                <?php } ?>


                                <?php
                                if ($axil_options['phone']) {
                                ?>
                                    <p>
                                        <a class="tel" href="tel:<?php echo wp_kses_post($phoneNumber); ?>"><i class="fas fa-phone"></i><?php echo wp_kses_post($axil_options['phone']); ?>
                                        </a>
                                    </p>
                                <?php
                                }
                                ?>
                                <?php
                                if ($axil_options['fax']) {
                                ?>
                                    <p>
                                        <a class="tel" href="tel:<?php echo wp_kses_post($faxNumber); ?>"><i class="fas fa-fax"></i><?php echo wp_kses_post($axil_options['fax']); ?>
                                        </a>
                                    </p>
                                <?php
                                }
                                ?>
                                <?php
                                if ($axil_options['email']) {
                                ?>
                                    <p>
                                        <a class="tel" href="mailto:<?php echo wp_kses_post($axil_options['email']); ?>"><i class="fas fa-envelope"></i><?php echo wp_kses_post($axil_options['email']); ?>
                                        </a>
                                    </p>
                                <?php
                                }
                                ?>
                            </address>
                            <address class="address">
                                <?php
                                if ($axil_options['address_field_title']) { ?>
                                    <span class="title"> <?php echo wp_kses_post($axil_options['address_field_title']); ?> </span>
                                <?php } ?>
                                <?php
                                if ($axil_options['address']) {
                                ?>
                                    <p class="m-b-xs-30 mid grey-dark-three"><?php echo wp_kses_post($axil_options['address']); ?></p>
                                <?php
                                }
                                ?>
                            </address>
                        </div>
                        <!-- End Single Address  -->
                    </div>
                    <!-- End Social Icon -->
                </div>
            </div>
            <!-- End Right Bar  -->
        </div>
    </div>
    <!-- End Side Content  -->
   
</div>
</div>
<!-- End Sidebar Area  -->