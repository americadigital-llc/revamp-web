<?php
/**
 * Template part for displaying header layout two
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

// Get Value
$axil_options = Helper::axil_get_options();
$header_layout = Helper::axil_header_layout();
$header_sticky = $header_layout['header_sticky'];
$header_transparent = $header_layout['header_transparent'];
// Condition

$header_sticky = ("no" !== $header_sticky && "0" !== $header_sticky) ? " axil-header-sticky " : "";
$header_transparent = ("no" !== $header_transparent && "0" !== $header_transparent) ? " header-transparent " : "";
// Menu
$axil_nav_menu_args = Helper::axil_nav_menu_args();
$axil_offcanvas_menu_args = Helper::axil_offcanvas_menu_args();


?>
<!-- Start Header -->
<header class="ax-header haeder-default  light-logo-version header-style-2 <?php echo esc_attr($header_sticky) ?>  <?php echo esc_attr($header_transparent) ?>">
    <div class="header-wrapper">
        <div class="container-fluid plr--100 plr_lg--30 plr_md--30 plr_sm--10">
            <div class="row align-items-center">
                <div class="col-lg-3 col-xl-3 col-md-6 col-sm-6 col-8 header-left">
                    <div class="logo">
                        <?php if (isset($axil_options['axil_logo_type'])): ?>
                            <a href="<?php echo esc_url(home_url('/')); ?>"
                               title="<?php echo esc_attr(get_bloginfo('name')); ?>" rel="home">

                                <?php if ('image' == $axil_options['axil_logo_type']): ?>

                                    <img src="<?php echo esc_url($axil_options['axil_head_logo']['url']); ?>"
                                         alt="<?php echo esc_attr(get_bloginfo('name')); ?>">

                                <?php else: ?>

                                    <?php if ('text' == $axil_options['axil_logo_type']): ?>

                                        <?php echo esc_html($axil_options['axil_logo_text']); ?>

                                    <?php endif ?>

                                <?php endif ?>

                            </a>
                        <?php else: ?>

                            <h3>
                                <a href="<?php echo esc_url(home_url('/')); ?>"
                                   title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
                                    <?php if (isset($axil_options['axil_logo_text']) ? $axil_options['axil_logo_text'] : '') {
                                        echo esc_html($axil_options['axil_logo_text']);
                                    } else {
                                        bloginfo('name');
                                    }
                                    ?>
                                </a>
                            </h3>

                            <?php $description = get_bloginfo('description', 'display');

                            if ($description || is_customize_preview()) { ?>

                                <p class="site-description"><?php echo esc_html($description); ?> </p>

                            <?php } ?>

                        <?php endif ?>

                    </div>
                </div>
                <div class="col-lg-8 col-xl-8 d-none d-lg-block">
                    <div class="mainmenu-wrapepr justify-content-center">
                        <!-- Start Mainmanu Nav -->
                        <?php if (has_nav_menu('primary')) {
                            wp_nav_menu($axil_nav_menu_args);
                        } ?>
                        <!-- End Mainmanu Nav -->
                    </div>
                </div>
                <div class="col-lg-1 col-xl-1 col-md-6 col-sm-6 col-4 header-right">

                    <!-- Start Header Extra  -->
                    <div class="axil-header-extra d-flex align-items-center justify-content-end">

                        <?php if ($axil_options['axil_enable_header_offcanvas_menu_icon']) { ?>
                            <!-- Start Hamburger -->
                            <div class="ax-hamburger ml--40 ml_sm--10 d-none d-lg-block">
                                <a class="axil-menuToggle ax-hamburger-trigger" id="side-nav-toggler" href="#">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </a>
                            </div>
                            <!-- End Hamburger -->
                        <?php } ?>

                        <!-- Start Menu Bar  -->
                        <div class="ax-menubar popup-navigation-activation d-block d-lg-none ml_sm--20 ml_md--20">
                            <div>
                                <i></i>
                            </div>
                        </div>
                        <!-- End Menu Bar  -->


                    </div>
                    <!-- End Header Extra  -->

                </div>
            </div>
        </div>
    </div>
</header>
<!-- Start Header -->
