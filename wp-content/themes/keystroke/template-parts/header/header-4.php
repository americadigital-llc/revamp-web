<?php
/**
 * Template part for displaying header layout four
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

// Get Value
$axil_options = Helper::axil_get_options();
$header_layout = Helper::axil_header_layout();
$header_sticky = $header_layout['header_sticky'];
$header_transparent = $header_layout['header_transparent'];
// Condition

$header_sticky = ("no" !== $header_sticky && "0" !== $header_sticky) ? " axil-header-sticky " : "";
$header_transparent = ("no" !== $header_transparent && "0" !== $header_transparent) ? " header-transparent " : "";
// Menu
$axil_nav_menu_args = Helper::axil_nav_menu_args();
$axil_offcanvas_menu_args = Helper::axil_offcanvas_menu_args();


?>
<!-- Start Header -->
<header class="ax-header haeder-default light-logo-version header-style-4 <?php echo esc_attr($header_sticky) ?>  <?php echo esc_attr($header_transparent) ?>">
    <div class="header-wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-6 col-sm-6 col-8">
                    <div class="logo">
                        <?php if (isset($axil_options['axil_logo_type'])): ?>
                            <a href="<?php echo esc_url(home_url('/')); ?>"
                               title="<?php echo esc_attr(get_bloginfo('name')); ?>" rel="home">

                                <?php if ('image' == $axil_options['axil_logo_type']): ?>

                                    <img src="<?php echo esc_url($axil_options['axil_head_logo']['url']); ?>"
                                         alt="<?php echo esc_attr(get_bloginfo('name')); ?>">

                                <?php else: ?>

                                    <?php if ('text' == $axil_options['axil_logo_type']): ?>

                                        <?php echo esc_html($axil_options['axil_logo_text']); ?>

                                    <?php endif ?>

                                <?php endif ?>

                            </a>
                        <?php else: ?>

                            <h3>
                                <a href="<?php echo esc_url(home_url('/')); ?>"
                                   title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
                                    <?php if (isset($axil_options['axil_logo_text']) ? $axil_options['axil_logo_text'] : '') {
                                        echo esc_html($axil_options['axil_logo_text']);
                                    } else {
                                        bloginfo('name');
                                    }
                                    ?>
                                </a>
                            </h3>

                            <?php $description = get_bloginfo('description', 'display');

                            if ($description || is_customize_preview()) { ?>

                                <p class="site-description"><?php echo esc_html($description); ?> </p>

                            <?php } ?>

                        <?php endif ?>

                    </div>
                </div>
                <div class="col-lg-9 col-md-6 col-sm-6 col-4">
                    <div class="mainmenu-wrapepr">
                        <!-- Start Mainmanu Nav -->
                        <?php if (has_nav_menu('primary')) {
                            wp_nav_menu($axil_nav_menu_args);
                        } ?>
                        <!-- End Mainmanu Nav -->

                        <?php if($axil_options['axil_header_btn']){?>
                            <div class="ax-header-button ml--40 ml_lg--10 d-none d-sm-block">
                                <a class="axil-button btn-solid btn-extra02-color" href="<?php echo esc_url($axil_options['axil_header_buttonUrl']); ?>">
                                    <span class="button-text"><?php echo esc_html( $axil_options['axil_header_buttontext'] ); ?></span><span class="button-icon"></span>
                                </a>
                            </div>
                        <?php } ?>

                        <!-- Start Menu Bar  -->
                        <div class="ax-menubar popup-navigation-activation d-block d-lg-none ml_sm--20 ml_md--20">
                            <div>
                                <i></i>
                            </div>
                        </div>
                        <!-- End Menu Bar  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Start Header -->
