<?php
/**
 * Template part for displaying main header
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

$axil_options = Helper::axil_get_options();
$header_layout = Helper::axil_header_layout();
$header_area = $header_layout['header_area'];
$header_style = $header_layout['header_style'];

/**
 * Load Header
 */
if ("no" !== $header_area && "0" !== $header_area) {
    get_template_part('template-parts/header/header', $header_style);
}
/**
 * Load Mobile Menu
 */
get_template_part('template-parts/header/mobile-menu');
/**
 * Load Off-Canvas
 */
get_template_part('template-parts/header/side-nav');
/**
 * Load Page Title Wrapper
 */
get_template_part('template-parts/title/title-wrapper');


?>
<!-- Start Page Wrapper -->
<main class="page-wrappper">


