<?php
/**
 * Template part for displaying header layout three
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

// Get Value
$axil_options = Helper::axil_get_options();
$header_layout = Helper::axil_header_layout();
$header_sticky = $header_layout['header_sticky'];
$header_transparent = $header_layout['header_transparent'];
// Condition

$header_sticky = ("no" !== $header_sticky && "0" !== $header_sticky) ? " axil-header-sticky " : "";
$header_transparent = ("no" !== $header_transparent && "0" !== $header_transparent) ? " header-transparent " : "";
// Menu
$axil_nav_menu_args = Helper::axil_nav_menu_args();
$axil_offcanvas_menu_args = Helper::axil_offcanvas_menu_args();

?>
<!-- Start Header -->
<header class="ax-header haeder-default light-logo-version header-style-3 <?php echo esc_attr($header_sticky) ?>  <?php echo esc_attr($header_transparent) ?>">
    <div class="header-wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 col-md-6 col-sm-6 col-8 header-left">
                    <div class="logo">
                        <?php if (isset($axil_options['axil_logo_type'])): ?>
                            <a href="<?php echo esc_url(home_url('/')); ?>"
                               title="<?php echo esc_attr(get_bloginfo('name')); ?>" rel="home">

                                <?php if ('image' == $axil_options['axil_logo_type']): ?>

                                    <img src="<?php echo esc_url($axil_options['axil_head_logo']['url']); ?>"
                                         alt="<?php echo esc_attr(get_bloginfo('name')); ?>">

                                <?php else: ?>

                                    <?php if ('text' == $axil_options['axil_logo_type']): ?>

                                        <?php echo esc_html($axil_options['axil_logo_text']); ?>

                                    <?php endif ?>

                                <?php endif ?>

                            </a>
                        <?php else: ?>

                            <h3>
                                <a href="<?php echo esc_url(home_url('/')); ?>"
                                   title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
                                    <?php if (isset($axil_options['axil_logo_text']) ? $axil_options['axil_logo_text'] : '') {
                                        echo esc_html($axil_options['axil_logo_text']);
                                    } else {
                                        bloginfo('name');
                                    }
                                    ?>
                                </a>
                            </h3>

                            <?php $description = get_bloginfo('description', 'display');

                            if ($description || is_customize_preview()) { ?>

                                <p class="site-description"><?php echo esc_html($description); ?> </p>

                            <?php } ?>

                        <?php endif ?>

                    </div>
                </div>
                <div class="col-lg-8 col-md-6 col-sm-6 col-4 header-right">
                    <div class="mainmenu-wrapepr">
                        <!-- Start Header Extra  -->
                        <div class="axil-header-extra d-flex align-items-center">
                            <?php if(!empty($axil_options['axil_social_icons'])){ ?>
                            <!-- Start Social Icons  -->
                            <div class="d-none d-md-block">
                                <ul class="axil-social-icons d-flex liststyle align-items-center">
                                    <?php
                                        foreach ($axil_options['axil_social_icons'] as $key => $value) {
                                            if ($value != '') {
                                                echo '<li><a class="' . esc_attr($key) . ' social-icon" href="' . esc_url($value) . '" target="_blank"><i class="fab fa-' . esc_attr($key) . '"></i></a></li>';
                                            }
                                        }
                                    ?>
                                </ul>
                            </div><!-- End Social Icons  -->
                            <?php } ?>

                            <?php if ($axil_options['axil_enable_header_offcanvas_menu_icon']) { ?>
                                <!-- Start Hamburger -->
                                <div class="ax-hamburger bg-theme-color ml--40">
                                    <a class="axil-menuToggle popup-navigation-activation" href="#">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </a>
                                </div>
                                <!-- End Hamburger -->
                            <?php } ?>
                        </div>
                        <!-- End Header Extra  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Start Header -->
