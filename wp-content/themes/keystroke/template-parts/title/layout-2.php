<?php
/**
 * Template part for displaying page banner style two
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

// Get Value
$axil_options = Helper::axil_get_options();
$banner_layout = Helper::axil_banner_layout();
$banner_area = $banner_layout['banner_area'];
$banner_style = $banner_layout['banner_style'];
$banner_title = axil_get_acf_data("axil_custom_title");
$banner_sub_title = axil_get_acf_data("axil_custom_sub_title");
$upload_banner_second_image = axil_get_acf_data("upload_banner_second_image");
// Get $post if you're inside a function.
global $post;
?>
<!-- Start Breadcrumb Area -->
<div class="axil-breadcrumb-area breadcrumb-style-2 single-service pt--170 pb--70 theme-gradient">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 order-2 order-lg-1 mt_md--30 mt_sm--30">
                <div class="inner">
                    <?php
                    if ($banner_title) { ?>
                        <h1 class="title"><?php echo wp_kses_post($banner_title); ?></h1>
                    <?php } else { ?>
                        <h1 class="title"><?php wp_title(''); ?></h1>
                    <?php } ?>

                    <?php if ($banner_sub_title) { ?>
                        <p><?php echo wp_kses_post($banner_sub_title); ?></p>
                    <?php } elseif (has_excerpt($post->ID)) { ?>
                        <?php the_excerpt(); ?>
                    <?php } else {
                        //Noting
                    } ?>
                </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2">
                <div class="thumbnail text-left text-lg-right">
                    <div class="image-group text-right">

                        <?php

                        $thumbnail_size = (is_singular('project') || is_singular('case-study')) ? "axil-project-banner-thumb" : "full";

                        if (has_post_thumbnail()) {
                            the_post_thumbnail($thumbnail_size, array('class' => 'image-1 paralax-image'));
                        } else { ?>
                            <img class="image-1 paralax-image"
                                 src="<?php echo get_template_directory_uri(); ?>/assets/images/slider/white-shape.png"
                                 alt="Slider images">
                        <?php } ?>

                        <?php
                        if ($upload_banner_second_image) { ?>
                            <img class="image-2 paralax-image"
                                 src="<?php echo esc_url($upload_banner_second_image['url']); ?>"
                                 alt="<?php echo esc_attr($upload_banner_second_image['alt']); ?>"/>
                        <?php } ?>
                    </div>
                    <div class="shape-group">
                        <div class="shape shape-1">
                            <span class="icon icon-breadcrumb-1"></span>
                        </div>
                        <div class="shape shape-2">
                            <span class="icon icon-breadcrumb-2"></span>
                        </div>
                        <div class="shape shape-3 customOne">
                            <span class="icon icon-breadcrumb-3"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Breadcrumb Area -->