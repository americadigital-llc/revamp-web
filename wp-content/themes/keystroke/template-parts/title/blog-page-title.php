<?php
/**
 * Template part for displaying page banner style one
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

// Get Value
$axil_options = Helper::axil_get_options();
$banner_layout = Helper::axil_banner_layout();
$banner_area = $banner_layout['banner_area'];
$banner_style = $banner_layout['banner_style'];
?>
<!-- Start Breadcrumb Area -->
<div class="axil-breadcrumb-area breadcrumb-style-default pt--170 pb--70 theme-gradient">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner">
                    <?php
                    if (is_single()){
                        if ($axil_options['axil_enable_single_post_breadcrumb_wrap'] !== 'hide'){
                            axil_breadcrumbs();
                        }
                    }elseif( !is_front_page() && is_page() ){
                        axil_breadcrumbs();
                    }else{
                        axil_breadcrumbs();
                    }
                    ?>
                    <?php
                    if ( is_home() ) {
                        if($axil_options['axil_enable_blog_title'] !== 'no'){
                            get_template_part('/template-parts/title/blog-title');
                        }
                    } elseif( is_search() ) {
                        get_template_part('/template-parts/title/blog-title');
                    } elseif( !is_front_page() && is_page() ) {
                        get_template_part('/template-parts/title/page-title');
                    } elseif(is_archive()) {
                        get_template_part('/template-parts/title/blog-title');
                    } elseif(is_single()) {
                        if($axil_options['axil_enable_single_post_title'] !== 'hide'){
                            get_template_part('/template-parts/title/single-post-title');
                        }
                    } else {
                        // Nothing
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="shape-images">
        <img class="shape shape-1" src="<?php echo get_template_directory_uri(); ?>/assets/images/shape/bcm-01.svg" alt="Shape Images">
        <div class="shape shape-2">
            <?php echo get_template_part( 'assets/images/shape/bcm-02'); ?>
        </div>
        <div class="shape shape-3">
            <?php echo get_template_part( 'assets/images/shape/bcm-03'); ?>
        </div>
    </div>
</div>
<!-- End Breadcrumb Area -->