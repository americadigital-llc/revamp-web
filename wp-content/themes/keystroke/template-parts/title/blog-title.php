<?php
/**
 * Template part for displaying header page title
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

// Get Value
$axil_options = Helper::axil_get_options();
?>
<!-- Start Breadcrumb Area -->
<div class="axil-breadcrumb-area breadcrumb-style-default pt--170 pb--70 theme-gradient">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner">
                    <?php  if ( !is_home() && "hide" !== $axil_options['axil_enable_single_post_breadcrumb_wrap']) {
                        axil_breadcrumbs();
                    } ?>
                    <?php 
                    if($axil_options['axil_enable_blog_title'] !== 'no'){
                        if ( is_archive() ): ?>
                            <h1 class="axil-page-title"><?php the_archive_title(); ?></h1>
                        <?php elseif( is_search() ): ?>
                            <h1 class="axil-page-title"><?php esc_html_e( 'Search results for: ', 'keystroke' ) ?><?php echo get_search_query(); ?></h1>
                        <?php else: ?>
                        <h1 class="axil-page-title">
                            <?php  if ( isset( $axil_options['axil_blog_text'] ) && !empty( $axil_options['axil_blog_text'] ) ){
                                echo esc_html( $axil_options['axil_blog_text'] );
                            } else {
                                echo esc_html__('Blogs', 'keystroke');
                            }  ?>
                        </h1>
                        <?php endif;
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="shape-images">
        <img class="shape shape-1" src="<?php echo get_template_directory_uri(); ?>/assets/images/shape/bcm-01.svg" alt="Shape Images">
        <div class="shape shape-2">
            <?php echo get_template_part( 'assets/images/shape/bcm-02'); ?>
        </div>
        <div class="shape shape-3">
            <?php echo get_template_part( 'assets/images/shape/bcm-03'); ?>
        </div>
    </div>
</div>
<!-- End Breadcrumb Area -->
