<?php
/**
 * Template part for displaying header page title
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

// Get Value
$axil_options = Helper::axil_get_options();
?>
<!-- Start Breadcrumb Area -->
<div class="axil-breadcrumb-area breadcrumb-style-default pt--170 pb--70 theme-gradient">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner">
                    <?php  if ("hide" !== $axil_options['axil_enable_single_post_breadcrumb_wrap']) {
                        axil_breadcrumbs();
                    } ?>
                    <!--  Title here  -->
                </div>
            </div>
        </div>
    </div>
    <div class="shape-images">
        <img class="shape shape-1" src="<?php echo get_template_directory_uri(); ?>/assets/images/shape/bcm-01.svg" alt="Shape Images">
        <div class="shape shape-2">
            <?php echo get_template_part( 'assets/images/shape/bcm-02'); ?>
        </div>
        <div class="shape shape-3">
            <?php echo get_template_part( 'assets/images/shape/bcm-03'); ?>
        </div>
    </div>
</div>
<!-- End Breadcrumb Area -->

