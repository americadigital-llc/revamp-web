<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

$images = axil_get_acf_data("axil_gallery_image");
$axil_options = Helper::axil_get_options();
$thumb_size = ($axil_options['axil_blog_sidebar'] === 'no') ? 'axil-single-blog-thumb':'axil-blog-thumb';
?>
<!-- Start Single Blog  -->
<div id="post-<?php the_ID(); ?>" <?php post_class('axil-blog-list gallery-post mt--80 mt_md--30 mt_sm--30 mt_lg--50'); ?>>
    <div class="blog-top">
        <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <?php Helper::axil_postmeta(); ?>
    </div>

    <?php
    if( $images ): ?>
        <div class="thumbnail axil-gallery-post axil-slick-dotstyle axil-arrow-button botton-bottom-transparent">
            <?php foreach( $images as $image ): ?>
                <div class="thumb-inner">
                    <a href="<?php the_permalink(); ?>">
                        <img class="w-100"  src="<?php echo esc_url($image['sizes'][$thumb_size]); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <div class="content">
        <?php the_excerpt(); ?>
        <?php Helper::axil_read_more(); ?>
    </div>
</div>
<!-- End Single Blog  -->