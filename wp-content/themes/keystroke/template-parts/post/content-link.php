<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

$custom_link = axil_get_acf_data("axil_custom_link");
$link = !empty($custom_link) ? $custom_link : get_the_permalink();
$axil_options = Helper::axil_get_options();
?>
<!-- Start Single Blog  -->
<div id="post-<?php the_ID(); ?>" <?php post_class('axil-blog-list sticky-blog mt--80 mt_md--30 mt_sm--30 mt_lg--50'); ?>>
    <div class="blog-top">
        <h3 class="title"><a href="<?php echo esc_url($link); ?>"><?php the_title(); ?></a></h3>
        <div class="info">
            <div class="info d-flex align-items-center">
                <?php
                if( $axil_options['axil_show_post_author_meta'] != 'no' ){ ?>
                    <h6 class="mb--0 pr--10"><?php the_author(); ?></h6>
                <?php } ?>
                <ul class="blog-meta d-flex align-items-center liststyle">
                    <?php if($axil_options['axil_show_post_publish_date_meta'] !== 'no' ){ ?>
                        <li><?php echo get_the_time(get_option('date_format')); ?></li>
                    <?php } ?>
                    <?php if($axil_options['axil_show_post_updated_date_meta'] !== 'no' ){ ?>
                        <li><?php echo the_modified_time(get_option('date_format')); ?></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="sticky">
            <i class="fas fa-link"></i>
        </div>
        <div class="shape-group">
            <div class="shape shape-1">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/others/shape-17.svg" alt="Shape Image">
            </div>
            <div class="shape shape-2">
                <?php echo get_template_part('assets/images/others/shape-18'); ?>
            </div>

        </div>
    </div>
</div>
<!-- End Single Blog  -->