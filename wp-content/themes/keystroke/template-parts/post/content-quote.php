<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

$axil_quote_author_name = axil_get_acf_data("axil_quote_author_name");
$axil_quote_author = !empty($axil_quote_author_name) ? $axil_quote_author_name : get_the_author();
$axil_quote_author_name_designation = axil_get_acf_data("axil_quote_author_name_designation");

?>
<!-- Start Single Blog  -->
<div id="post-<?php the_ID(); ?>" <?php post_class('axil-blog-list quote-blog mt--80 mt_md--30 mt_sm--30 mt_lg--50'); ?>>
    <div class="blog-top">
        <blockquote>
            <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        </blockquote>
        <div class="author">
            <div class="info">
                <h6><?php echo esc_html($axil_quote_author); ?></h6>
                <ul class="blog-meta">
                    <li><?php echo esc_html($axil_quote_author_name_designation); ?></li>
                </ul>
            </div>
        </div>
        <div class="shape-group">
            <div class="shape shape-1">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/others/shape-17.svg" alt="Shape Image">
            </div>
            <div class="shape shape-2">
                <?php echo get_template_part('assets/images/others/shape-18'); ?>
            </div>
            <div class="shape shape-3">
                <?php echo get_template_part( 'assets/images/others/quote'); ?>
            </div>
        </div>
    </div>
</div>
<!-- End Single Blog  -->

