<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */
$axil_options = Helper::axil_get_options();
$thumb_size = ($axil_options['axil_blog_sidebar'] === 'no') ? 'axil-single-blog-thumb':'axil-blog-thumb';
?>
<!-- Start Single Blog  -->
<div id="post-<?php the_ID(); ?>" <?php post_class('axil-blog-list mt--80 mt_md--30 mt_sm--30 mt_lg--50'); ?>>
    <div class="blog-top">
        <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <?php Helper::axil_postmeta(); ?>
    </div>
    <?php if(has_post_thumbnail()){ ?>
        <div class="thumbnail">
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail($thumb_size) ?>
            </a>
        </div>
    <?php } ?>

    <div class="content">
        <?php the_excerpt(); ?>
        <?php Helper::axil_read_more(); ?>
    </div>
</div>
<!-- End Single Blog  -->
