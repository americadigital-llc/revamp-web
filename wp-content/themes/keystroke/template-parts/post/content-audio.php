<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package keystroke
 */

$audio_url = axil_get_acf_data("axil_upload_audio");
$axil_options = Helper::axil_get_options();
$thumb_size = ($axil_options['axil_blog_sidebar'] === 'no') ? 'axil-single-blog-thumb':'axil-blog-thumb';
?>
<!-- Start Single Blog  -->
<div id="post-<?php the_ID(); ?>" <?php post_class('axil-blog-list mt--80 mt_md--30 mt_sm--30 mt_lg--50'); ?>>
    <div class="blog-top">
        <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <?php Helper::axil_postmeta(); ?>
    </div>
    <?php if(has_post_thumbnail()){ ?>
        <div class="thumbnail position-relative">
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail($thumb_size, ['class' => 'w-100']) ?>
            </a>
        </div>
    <?php } ?>

    <?php if( $audio_url ): ?>
        <audio controls>
            <source src="<?php echo esc_url($audio_url['url']); ?>" type="audio/ogg">
            <source src="<?php echo esc_url($audio_url['url']); ?>" type="audio/mpeg">
            <?php esc_html_e('Your browser does not support the audio tag.', 'keystroke'); ?>
        </audio>
    <?php endif; ?>


    <div class="content">
        <p><?php the_excerpt(); ?></p>
        <?php Helper::axil_read_more(); ?>
    </div>
</div>
<!-- End Single Blog  -->