<?php
/**
 * @author  Axilweb
 * @since   1.0
 * @version 1.0
 * @package keystroke
 */

trait PostMeta
{
    // Post Meta
    public static function axil_postmeta()
    {
        $axil_options = Helper::axil_get_options();
        ?>
        <div class="author">
            <?php
            if ($axil_options['axil_show_post_author_meta'] != 'no') {
                echo get_avatar(get_the_author_meta('ID'), 50);
            }
            ?>
            <div class="info">
                <?php
                if ($axil_options['axil_show_post_author_meta'] != 'no') { ?>
                    <h6><?php the_author(); ?></h6>
                <?php } ?>
                <ul class="blog-meta">
                    <?php if ($axil_options['axil_show_post_publish_date_meta'] !== 'no') { ?>
                        <li class="post-meta-date"><?php echo get_the_time(get_option('date_format')); ?></li>
                    <?php } ?>
                    <?php if ($axil_options['axil_show_post_updated_date_meta'] !== 'no') { ?>
                        <li class="post-meta-update-date"><?php echo the_modified_time(get_option('date_format')); ?></li>
                    <?php } ?>
                    <?php if ($axil_options['axil_show_post_reading_time_meta'] !== 'no') { ?>
                        <li class="post-meta-reading-time"><?php echo keystroke_content_estimated_reading_time(get_the_content()); ?></li>
                    <?php } ?>
                    <?php if ($axil_options['axil_show_post_comments_meta'] !== 'no') { ?>
                        <li class="post-meta-comments"><?php comments_popup_link(esc_html__('No Comments', 'keystroke'), esc_html__('1 Comment', 'keystroke'), esc_html__('% Comments', 'keystroke'), 'post-comment', esc_html__('Comments off', 'keystroke')); ?></li>
                    <?php } ?>
                    <?php if ($axil_options['axil_show_post_categories_meta'] !== 'no' && has_category()) { ?>
                        <li class="post-meta-categories"><?php the_category(', &nbsp;'); ?></li>
                    <?php } ?>
                    <?php if ($axil_options['axil_show_post_tags_meta'] !== 'no' && has_tag()) { ?>
                        <li class="post-meta-tags"><?php the_tags(' ', ' '); ?></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    <?php }


    // Single post meta
    public static function axil_singlepostmeta()
    {
        $axil_options = Helper::axil_get_options();
        ?>
        <div class="author">
            <?php
            if (!empty($axil_options['axil_show_blog_details_author_meta']) && $axil_options['axil_show_blog_details_author_meta'] != 'no') {
                echo get_avatar(get_the_author_meta('ID'), 50);
            }
            ?>
            <div class="info">
                <?php
                if (!empty($axil_options['axil_show_blog_details_author_meta']) && $axil_options['axil_show_blog_details_author_meta'] != 'no') { ?>
                    <h6><?php the_author(); ?></h6>
                <?php } ?>
                <ul class="blog-meta">
                    <?php if ($axil_options['axil_show_blog_details_publish_date_meta'] !== 'no') { ?>
                        <li class="post-meta-date"><?php echo get_the_time(get_option('date_format')); ?></li>
                    <?php } ?>
                    <?php if ($axil_options['axil_show_blog_details_updated_date_meta'] !== 'no') { ?>
                        <li class="post-meta-update-date"><?php echo the_modified_time(get_option('date_format')); ?></li>
                    <?php } ?>
                    <?php if ($axil_options['axil_show_blog_details_reading_time_meta'] !== 'no') { ?>
                        <li class="post-meta-reading-time"><?php echo keystroke_content_estimated_reading_time(get_the_content()); ?></li>
                    <?php } ?>
                    <?php if ($axil_options['axil_show_blog_details_comments_meta'] !== 'no') { ?>
                        <li class="post-meta-comments"><?php comments_popup_link(esc_html__('No Comments', 'keystroke'), esc_html__('1 Comment', 'keystroke'), esc_html__('% Comments', 'keystroke'), 'post-comment', esc_html__('Comments off', 'keystroke')); ?></li>
                    <?php } ?>
                    <?php if ($axil_options['axil_show_post_categories_meta'] !== 'no' && has_category()) { ?>
                        <li class="post-meta-categories"><?php the_category(', &nbsp;'); ?></li>
                    <?php } ?>
                    <?php if ($axil_options['axil_show_post_tags_meta'] !== 'no' && has_tag()) { ?>
                        <li class="post-meta-tags"><?php the_tags(' ', ' '); ?></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    <?php }

    public static function axil_read_more()
    {
        $axil_options = Helper::axil_get_options();
        if ($axil_options['axil_enable_readmore_btn'] !== 'no') { ?>
            <a class="axil-button btn-large btn-transparent" href="<?php the_permalink(); ?>"><span
                        class="button-text"><?php echo esc_html($axil_options['axil_readmore_text'], 'keystroke') ?></span><span
                        class="button-icon"></span></a>
        <?php }
    }


}



