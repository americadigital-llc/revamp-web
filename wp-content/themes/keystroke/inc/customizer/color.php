<?php
/**
 * Contains methods for customizing the theme customization screen.
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 * @since axil 1.0
 */
/**
 * axil_custom_customize_register
 */
if (!function_exists('axil_custom_customize_register')) {
    function axil_custom_customize_register()
    {
        /**
         * Custom Separator
         */
        class Keystroke_Separator_Custom_control extends WP_Customize_Control
        {
            public $type = 'separator';

            public function render_content()
            {
                ?>
                <p>
                <hr></p>
                <?php
            }
        }
    }

    add_action('customize_register', 'axil_custom_customize_register');
}

/**
 * Start axil_Customize
 */
class axil_Customize
{
    /**
     * This hooks into 'customize_register' (available as of WP 3.4) and allows
     * you to add new sections and controls to the Theme Customize screen.
     *
     * Note: To enable instant preview, we have to actually write a bit of custom
     * javascript. See axil_live_preview() for more.
     *
     * @see add_action('customize_register',$func)
     * @param \WP_Customize_Manager $wp_customize
     * @link http://ottopress.com/2012/how-to-leverage-the-theme-customizer-in-your-own-themes/
     * @since axil 1.0
     */
    public static function register($wp_customize)
    {

        //1. Define a new section (if desired) to the Theme Customizer
        $wp_customize->add_panel('axil_colors_options',
            array(
                'title' => esc_html__('Keystroke Colors Options', 'keystroke'), //Visible title of section
                'priority' => 35, //Determines what order this appears in
                'capability' => 'edit_theme_options', //Capability needed to tweak
                'description' => esc_html__('Allows you to customize some example settings for axil.', 'keystroke'), //Descriptive tooltip
            )
        );

        $wp_customize->add_section('axil_colors_main_options',
            array(
                'title' => esc_html__('General', 'keystroke'), //Visible title of section
                'priority' => 10, //Determines what order this appears in
                'capability' => 'edit_theme_options', //Capability needed to tweak
                'panel' => 'axil_colors_options',
            )
        );

        /*************************
         * Primary
         ************************/
        $wp_customize->add_setting('color_primary',
            array(
                'default' => '#702FFF',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_primary',
            array(
                'label' => esc_html__('Primary Color', 'keystroke'),
                'settings' => 'color_primary',
                'priority' => 10,
                'section' => 'axil_colors_main_options',
            )
        ));

        /*************************
         * Primary Altr
         ************************/
        $wp_customize->add_setting('color_primary_alt',
            array(
                'default' => '#505CFD',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_primary_alt',
            array(
                'label' => esc_html__('Primary Alt Color (For Dotted Shape)', 'keystroke'),
                'settings' => 'color_primary_alt',
                'priority' => 10,
                'section' => 'axil_colors_main_options',
            )
        ));
        /**
         * Separator
         */
        $wp_customize->add_setting('axil_separator_heading_hover', array(
            'default' => '',
            'sanitize_callback' => 'esc_html',
        ));
        $wp_customize->add_control(new Keystroke_Separator_Custom_control($wp_customize, 'axil_separator_heading_hover', array(
            'settings' => 'axil_separator_heading_hover',
            'section' => 'axil_colors_main_options',
        )));

        /*************************
         * Secondary
         ************************/
        $wp_customize->add_setting('color_secondary',
            array(
                'default' => '#00D09C',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_secondary',
            array(
                'label' => esc_html__('Secondary Color', 'keystroke'),
                'settings' => 'color_secondary',
                'priority' => 10,
                'section' => 'axil_colors_main_options',
            )
        ));

        /*************************
         * Tertiary
         ************************/
        $wp_customize->add_setting('color_tertiary',
            array(
                'default' => '#FFDC71',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_tertiary',
            array(
                'label' => esc_html__('Tertiary Color', 'keystroke'),
                'settings' => 'color_tertiary',
                'priority' => 10,
                'section' => 'axil_colors_main_options',
            )
        ));



        /**
         * Separator
         */
        $wp_customize->add_setting('axil_separator_meta_color', array(
            'default' => '',
            'sanitize_callback' => 'esc_html',
        ));
        $wp_customize->add_control(new Keystroke_Separator_Custom_control($wp_customize, 'axil_separator_meta_color', array(
            'settings' => 'axil_separator_meta_color',
            'section' => 'axil_colors_main_options',
        )));

        // Button Color
        $wp_customize->add_setting('global_color_button',
            array(
                // 'default' => '#494e51',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_global_color_button',
            array(
                'label' => esc_html__('Button Color', 'keystroke'),
                'settings' => 'global_color_button',
                'priority' => 10,
                'section' => 'axil_colors_main_options',
            )
        ));

        /**
         * Separator
         */
        $wp_customize->add_setting('axil_separator_link_color', array(
            'default' => '',
            'sanitize_callback' => 'esc_html',
        ));
        $wp_customize->add_control(new Keystroke_Separator_Custom_control($wp_customize, 'axil_separator_link_color', array(
            'settings' => 'axil_separator_link_color',
            'section' => 'axil_colors_main_options',
        )));

        // Link Color
        $wp_customize->add_setting('color_link',
            array(
                // 'default' => '#494e51',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_link',
            array(
                'label' => esc_html__('Global Link Color', 'keystroke'),
                'settings' => 'color_link',
                'priority' => 10,
                'section' => 'axil_colors_main_options',
            )
        ));
        // Link Hover Color
        $wp_customize->add_setting('color_link_hover',
            array(
                // 'default' => '#494e51',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_link_hover',
            array(
                'label' => esc_html__('Global Link Hover Color', 'keystroke'),
                'settings' => 'color_link_hover',
                'priority' => 10,
                'section' => 'axil_colors_main_options',
            )
        ));


        /**
         * Separator
         */
        $wp_customize->add_setting('axil_separator_heading_hover', array(
            'default' => '',
            'sanitize_callback' => 'esc_html',
        ));
        $wp_customize->add_control(new Keystroke_Separator_Custom_control($wp_customize, 'axil_separator_heading_hover', array(
            'settings' => 'axil_separator_heading_hover',
            'section' => 'axil_colors_main_options',
        )));
        // Heading Color
        $wp_customize->add_setting('color_heading',
            array(
                // 'default' => '#000248',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_heading',
            array(
                'label' => esc_html__('Heading Color', 'keystroke'),
                'settings' => 'color_heading',
                'priority' => 10,
                'section' => 'axil_colors_main_options',
            )
        ));

        /**
         * Separator
         */
        $wp_customize->add_setting('axil_separator_body_color', array(
            'default' => '',
            'sanitize_callback' => 'esc_html',
        ));
        $wp_customize->add_control(new Keystroke_Separator_Custom_control($wp_customize, 'axil_separator_body_color', array(
            'settings' => 'axil_separator_body_color',
            'section' => 'axil_colors_main_options',
        )));

        // Body Color
        $wp_customize->add_setting('color_body',
            array(
                // 'default' => '#494e51',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_body',
            array(
                'label' => esc_html__('Body Color', 'keystroke'),
                'settings' => 'color_body',
                'priority' => 10,
                'section' => 'axil_colors_main_options',
            )
        ));




        /**
         * Separator
         */
        $wp_customize->add_setting('axil_separator_meta_color2', array(
            'default' => '',
            'sanitize_callback' => 'esc_html',
        ));
        $wp_customize->add_control(new Keystroke_Separator_Custom_control($wp_customize, 'axil_separator_meta_color2', array(
            'settings' => 'axil_separator_meta_color2',
            'section' => 'axil_colors_main_options',
        )));

        // Meta Color
        $wp_customize->add_setting('color_meta',
            array(
                // 'default' => '#7b7b7b',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_meta',
            array(
                'label' => esc_html__('Meta Text Color', 'keystroke'),
                'settings' => 'color_meta',
                'priority' => 10,
                'section' => 'axil_colors_main_options',
            )
        ));
        // Meta Color
        $wp_customize->add_setting('link_color_meta',
            array(
                // 'default' => '#7b7b7b',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_link_color_meta',
            array(
                'label' => esc_html__('Meta Link Color', 'keystroke'),
                'settings' => 'link_color_meta',
                'priority' => 10,
                'section' => 'axil_colors_main_options',
            )
        ));
        // Meta hover Color
        $wp_customize->add_setting('color_meta_hover',
            array(
                // 'default' => '#702FFF',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_meta_hover',
            array(
                'label' => esc_html__('Meta Link Hover Color', 'keystroke'),
                'settings' => 'color_meta_hover',
                'priority' => 10,
                'section' => 'axil_colors_main_options',
            )
        ));

        /*************************
         * Header Color
         ************************/

        $wp_customize->add_section('axil_colors_header_options',
            array(
                'title' => esc_html__('Header', 'keystroke'), //Visible title of section
                'priority' => 10, //Determines what order this appears in
                'capability' => 'edit_theme_options', //Capability needed to tweak
                'panel' => 'axil_colors_options',
            )
        );
        // Link Color
        $wp_customize->add_setting('color_header_link_color',
            array(
                // 'default' => '#121213',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_header_link_color',
            array(
                'label' => esc_html__('Navigation Link Color', 'keystroke'),
                'settings' => 'color_header_link_color',
                'priority' => 10,
                'section' => 'axil_colors_header_options',
            )
        ));
        // Link Hover Color
        $wp_customize->add_setting('color_header_link_hover_color',
            array(
                // 'default' => '#702FFF',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_header_link_hover_color',
            array(
                'label' => esc_html__('Navigation Link Hover + Active Color', 'keystroke'),
                'settings' => 'color_header_link_hover_color',
                'priority' => 10,
                'section' => 'axil_colors_header_options',
            )
        ));
        // BG Color
        $wp_customize->add_setting('color_header_bg',
            array(
                // 'default' => '',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_header_bg',
            array(
                'label' => esc_html__('Navigation Background Color', 'keystroke'),
                'settings' => 'color_header_bg',
                'priority' => 10,
                'section' => 'axil_colors_header_options',
            )
        ));
        // Separetor Color
        $wp_customize->add_setting('color_dropdown_menu_separator',
            array(
                // 'default' => '',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_dropdown_menu_separator',
            array(
                'label' => esc_html__('Dropdown Menu Separator', 'keystroke'),
                'settings' => 'color_dropdown_menu_separator',
                'priority' => 10,
                'section' => 'axil_colors_header_options',
            )
        ));


        //  Footer
        $wp_customize->add_section('axil_colors_footer_options',
            array(
                'title' => esc_html__('Footer', 'keystroke'), //Visible title of section
                'priority' => 35, //Determines what order this appears in
                'capability' => 'edit_theme_options', //Capability needed to tweak
                'panel' => 'axil_colors_options',
            )
        );

        // Footer Heading Color
        $wp_customize->add_setting('color_footer_heading_color',
            array(
                // 'default' => '#ffffff',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_footer_heading_color',
            array(
                'label' => esc_html__('Title Color', 'keystroke'),
                'settings' => 'color_footer_heading_color',
                'priority' => 10,
                'section' => 'axil_colors_footer_options',
            )
        ));

        // Text Color
        $wp_customize->add_setting('color_footer_body_color',
            array(
                // 'default' => '#6b7074',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_footer_body_color',
            array(
                'label' => esc_html__('Text Color', 'keystroke'),
                'settings' => 'color_footer_body_color',
                'priority' => 10,
                'section' => 'axil_colors_footer_options',
            )
        ));

        // Link Color
        $wp_customize->add_setting('color_footer_link_color',
            array(
                // 'default' => '#6b7074',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_footer_link_color',
            array(
                'label' => esc_html__('Link Color', 'keystroke'),
                'settings' => 'color_footer_link_color',
                'priority' => 10,
                'section' => 'axil_colors_footer_options',
            )
        ));

        // Link Hover Color
        $wp_customize->add_setting('color_footer_link_hover_color',
            array(
                // 'default' => '#702FFF',
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_footer_link_hover_color',
            array(
                'label' => esc_html__('Link Hover Color', 'keystroke'),
                'settings' => 'color_footer_link_hover_color',
                'priority' => 10,
                'section' => 'axil_colors_footer_options',
            )
        ));

        // Footer Bottom Border Top Color
        $wp_customize->add_setting('color_footer_bottom_border_color',
            array(
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_footer_bottom_border_color',
            array(
                'label' => esc_html__('Footer Bottom Border Color', 'keystroke'),
                'settings' => 'color_footer_bottom_border_color',
                'priority' => 10,
                'section' => 'axil_colors_footer_options',
            )
        ));
        // Background Color
        $wp_customize->add_setting('color_footer_bg_color',
            array(
                'type' => 'theme_mod',
                'capability' => 'edit_theme_options',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_hex_color'
            )
        );
        $wp_customize->add_control(new WP_Customize_Color_Control(
            $wp_customize,
            'axil_color_footer_bg_color',
            array(
                'label' => esc_html__('Footer Background Color', 'keystroke'),
                'settings' => 'color_footer_bg_color',
                'priority' => 10,
                'section' => 'axil_colors_footer_options',
            )
        ));

    }

    /**
     * This will output the custom WordPress settings to the live theme's WP head.
     *
     * Used by hook: 'wp_head'
     *
     * @see add_action('wp_head',$func)
     * @since axil 1.0
     */
    public static function axil_custom_color_output()
    {
        ?>
        <!--Customizer CSS-->
        <style type="text/css">

            /* Body */
            <?php self::axil_generate_css('body, p, ol li, .axil-breadcrumb-area.breadcrumb-style-2 p, p.subtitle-1, p.subtitle-2, p.subtitle-3, .axil-pricing-table .pricing-body .inner ul.list-style li, .axil-pricing-table .pricing-header .price-wrapper .price span', 'color ', 'color_body'); ?>
            /* Link */
            <?php self::axil_generate_css('a, .ft-social-share li a, ul li a, a.axil-button.axil-link-button, button.axil-button.axil-link-button', 'color ', 'color_link'); ?>
            <?php self::axil_generate_css('a.axil-button.axil-link-button::after, button.axil-button.axil-link-button::after', 'background ', 'color_link'); ?>
            /* Link Hover */
            <?php self::axil_generate_css('a:hover, .ft-social-share li:hover a, ul li a:hover, a.axil-button.axil-link-button:hover, button.axil-button.axil-link-button:hover', 'color ', 'color_link_hover'); ?>
            <?php self::axil_generate_css('a.axil-button.axil-link-button:hover::after, button.axil-button.axil-link-button:hover::after', 'background ', 'color_link_hover'); ?>
            /* Meta */
            <?php self::axil_generate_css('ul.blog-meta li, .axil-single-widget .small-post .content ul.blog-meta li', 'color ', 'color_meta'); ?>
            /* Meta Hover */
            <?php self::axil_generate_css('.axil-blog-list .blog-top .author .info ul.blog-meta li a, .axil-blog-details-area .blog-top .author .info ul.blog-meta li a', 'color ', 'link_color_meta'); ?>
            /* Meta Link Hover */
            <?php self::axil_generate_css('.axil-blog-list .blog-top .author .info ul.blog-meta li a:hover, .axil-blog-details-area .blog-top .author .info ul.blog-meta li a:hover', 'color ', 'color_meta_hover'); ?>


            /************************************************************************************
             * Button
             ************************************************************************************/
            /* Color */
            <?php self::axil_generate_css('a.axil-button, button.axil-button, a.axil-button, button.axil-button, a.axil-button.btn-solid.btn-extra02-color:hover, button.axil-button.btn-solid.btn-extra02-color:hover', 'color ', 'global_color_button'); ?>
            <?php self::axil_generate_css('a.axil-button.btn-solid.btn-extra02-color:hover, button.axil-button.btn-solid.btn-extra02-color:hover, a.axil-button.btn-solid.btn-extra07-color:hover, button.axil-button.btn-solid.btn-extra07-color:hover, .elementor-206 .elementor-element.elementor-element-877fc97 .axil-button:hover', 'color ', 'global_color_button', '', ' !important'); ?>
            <?php self::axil_generate_css('a.axil-button.btn-transparent::before, button.axil-button.btn-transparent::before, a.axil-button.btn-solid.btn-extra02-color::after, button.axil-button.btn-solid.btn-extra02-color::after, a.axil-button.btn-solid.btn-extra07-color::after, button.axil-button.btn-solid.btn-extra07-color::after, .elementor-206 .elementor-element.elementor-element-877fc97 .axil-button:hover::after', 'border-color ', 'global_color_button'); ?>
            <?php self::axil_generate_css('a.axil-button.btn-transparent span.button-icon, button.axil-button.btn-transparent span.button-icon, a.axil-button.btn-solid.btn-extra02-color:hover span.button-icon, button.axil-button.btn-solid.btn-extra02-color:hover span.button-icon, a.axil-button.btn-solid.btn-extra07-color:hover span.button-icon, button.axil-button.btn-solid.btn-extra07-color:hover span.button-icon, .elementor-206 .elementor-element.elementor-element-877fc97 .axil-button:hover .button-icon', 'border-left-color ', 'global_color_button'); ?>
            <?php self::axil_generate_css('a.axil-button.btn-transparent::after, button.axil-button.btn-transparent::after, a.axil-button.btn-solid.btn-extra02-color::before, button.axil-button.btn-solid.btn-extra02-color::before, a.axil-button.btn-solid.btn-extra07-color::before, button.axil-button.btn-solid.btn-extra07-color::before, .elementor-206 .elementor-element.elementor-element-877fc97 .axil-button::before', 'background-color ', 'global_color_button', '', ' !important'); ?>


            /************************************************************************************
             * Header
             ************************************************************************************/
            /* Background Color */
            <?php self::axil_generate_css('.ax-header, .haeder-default.sticky, ul.mainmenu li.has-dropdown ul.axil-submenu', 'background-color ', 'color_header_bg', '', ' !important'); ?>
            /* Separator Color */
            <?php self::axil_generate_css('ul.mainmenu li.has-dropdown ul.axil-submenu li a', 'border-color  ', 'color_dropdown_menu_separator'); ?>
            /* Link Color */
            <?php self::axil_generate_css('ul.mainmenu li a, ul.mainmenu li.has-dropdown ul.axil-submenu li a', 'color ', 'color_header_link_color'); ?>
            /* Link Hover Color */
            <?php self::axil_generate_css('ul.mainmenu > li:hover > a, ul.mainmenu li.has-dropdown:hover > a, ul.mainmenu li.has-dropdown ul.axil-submenu li a:hover, ul.mainmenu > li.current_page_item > a, ul.mainmenu > li.is-active > a', 'color ', 'color_header_link_hover_color'); ?>


            /************************************************************************************
             * General
             ************************************************************************************/
            /* Primary [#702FFF] */
            <?php self::axil_generate_css(':root', '--color-primary', 'color_primary'); ?>
            <?php self::axil_generate_css(':root', '--color-primary-alt', 'color_primary_alt'); ?>
            <?php self::axil_generate_css(':root', '--color-secondary', 'color_secondary'); ?>
            <?php self::axil_generate_css(':root', '--color-tertiary', 'color_tertiary'); ?>
            /* Heading */
            <?php self::axil_generate_css('h1, .h1, h2, .h2, h3, .h3, h4, .h4, h5, .h5, h6, .h6', 'color ', 'color_heading'); ?>

            /************************************************************************************
             * Footer
             ************************************************************************************/
            /* Background Color */
            <?php self::axil_generate_css('.axil-footer-style-1 .bg_image--2, footer.axil-footer.footer-default.footer-style-3.bg-color-extra09.axil-footer-style-3, .axil-call-to-action.callaction-style-2.bg-color-extra09', 'background-color ', 'color_footer_bg_color'); ?>
            /* Footer Heading Color */
            <?php self::axil_generate_css('.footer-widget-item .title, .footer-default.footer-style-3 .footer-widget-item h6.title, .footer-default .footer-widget-item h2, .footer-widget-item h1, .footer-widget-item h3, .footer-widget-item h4, .footer-widget-item h5, .footer-widget-item h6 ', 'color ', 'color_footer_heading_color'); ?>
            /* Footer Body Color */
            <?php self::axil_generate_css('.footer-default.footer-style-3 .footer-widget-item .axil-ft-address .address p span.address-info, .footer-default.axil-footer-style-1 .footer-widget-item p, .footer-widget-item p, .copyright-default p', 'color ', 'color_footer_body_color'); ?>
            /* Footer Link Color */
            <?php self::axil_generate_css('.footer-widget-item ul.menu li a, .footer-widget-item a:not(.axil-button), .copyright-default .quick-contact ul li a, .copyright a', 'color ', 'color_footer_link_color'); ?>
            /* Footer Link Hover Color */
            <?php self::axil_generate_css('.footer-widget-item ul.menu li a:hover, .copyright-default .quick-contact ul li a:hover, .copyright a:hover', 'color ', 'color_footer_link_hover_color'); ?>
            /* Footer Bottom Border top Color */
            <?php self::axil_generate_css('.axil-basic-thine-line, .footer-default.footer-style-3 .axil-basic-thine-line', 'border-color ', 'color_footer_bottom_border_color'); ?>
            <?php self::axil_generate_css('.footer-default .footer-widget-item.axil-border-right::after', 'background ', 'color_footer_bottom_border_color'); ?>

        </style>
        <!--/Customizer CSS-->
        <?php
    }

    /**
     * This will generate a line of CSS for use in header output. If the setting
     * ($mod_name) has no defined value, the CSS will not be output.
     *
     * @uses get_theme_mod()
     * @param string $selector CSS selector
     * @param string $style The name of the CSS *property* to modify
     * @param string $mod_name The name of the 'theme_mod' option to fetch
     * @param string $prefix Optional. Anything that needs to be output before the CSS property
     * @param string $postfix Optional. Anything that needs to be output after the CSS property
     * @param bool $echo Optional. Whether to print directly to the page (default: true).
     * @return string Returns a single line of CSS with selectors and a property.
     * @since axil 1.0
     */
    public static function axil_generate_css($selector, $style, $mod_name, $prefix = '', $postfix = '', $echo = true)
    {
        $return = '';
        $mod = get_theme_mod($mod_name);
        if (!empty($mod)) {
            $return = sprintf('%s { %s:%s; }',
                $selector,
                $style,
                $prefix . $mod . $postfix
            );
            if ($echo) {
                echo awescapeing($return);
            }
        }
        return $return;
    }
}

// Setup the Theme Customizer settings and controls...
add_action('customize_register', array('axil_Customize', 'register'));

// Output custom CSS to live site
add_action('wp_head', array('axil_Customize', 'axil_custom_color_output'));


