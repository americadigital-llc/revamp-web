<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package keystroke
 */

/**
 * Enqueue scripts and styles.
 */

if (!function_exists('keystroke_scripts')){
    function keystroke_scripts() {

        wp_deregister_style('font-awesome');

        // Fonts
        wp_enqueue_style('axil-fonts',axil_fonts_url());
        wp_enqueue_style('axil-svg-icon', AXIL_THEME_URI . '/assets/svg-icon/style.css', array(), AXIL_VERSION);
        // Style


        wp_enqueue_style('bootstrap', AXIL_CSS_URL . 'vendor/bootstrap.min.css', array(), AXIL_VERSION);
        wp_enqueue_style('slick', AXIL_CSS_URL . 'plugins/slick.css', array(), AXIL_VERSION);
        wp_enqueue_style('font-awesome', AXIL_CSS_URL . 'plugins/font-awesome.css', array(), AXIL_VERSION);
        wp_enqueue_style('axil-style', AXIL_CSS_URL . 'style.css', array(), AXIL_VERSION);
        wp_enqueue_style('axil-development-extra-style', AXIL_CSS_URL . 'development-extra.css', array(), AXIL_VERSION);
        wp_enqueue_style( 'keystroke-style', get_stylesheet_uri() );


        
        // Scripts
        wp_enqueue_script('bootstrap', AXIL_JS_URL . 'vendor/bootstrap.min.js', array('jquery'), AXIL_VERSION, true);
        wp_enqueue_script('waypoints', AXIL_JS_URL . 'waypoints.min.js', array('jquery'), AXIL_VERSION, true);
        wp_enqueue_script('wow', AXIL_JS_URL . 'wow.js', array('jquery'), AXIL_VERSION, true);
        wp_enqueue_script('counterup', AXIL_JS_URL . 'counterup.js', array('jquery'), AXIL_VERSION, true);
        wp_enqueue_script('imagesloaded');
        wp_enqueue_script('isotope', AXIL_JS_URL . 'isotope.js', array('jquery'), AXIL_VERSION, true);
        wp_enqueue_script('tilt', AXIL_JS_URL . 'tilt.js', array('jquery'), AXIL_VERSION, true);
        wp_enqueue_script('tweenmax', AXIL_JS_URL . 'tweenmax.js', array('jquery'), AXIL_VERSION, true);
        wp_enqueue_script('slick', AXIL_JS_URL . 'slick.js', array('jquery'), AXIL_VERSION, true);
        wp_enqueue_script('youtube', AXIL_JS_URL . 'youtube.js', array('jquery'), AXIL_VERSION, true);
        wp_enqueue_script('scrollup', AXIL_JS_URL . 'scrollup.js', array('jquery'), AXIL_VERSION, true);
        wp_enqueue_script('stickysidebar', AXIL_JS_URL . 'stickysidebar.js', array('jquery'), AXIL_VERSION, true);
        wp_enqueue_script('axil-main', AXIL_JS_URL . 'main.js', array('jquery'), AXIL_VERSION, true);

        wp_enqueue_script( 'keystroke-navigation', AXIL_ADMIN_JS_URL . 'navigation.js', array(), AXIL_VERSION, true );
        wp_enqueue_script( 'keystroke-skip-link-focus-fix', AXIL_ADMIN_JS_URL . 'skip-link-focus-fix.js', array(), AXIL_VERSION, true );

        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }

    }
}
add_action( 'wp_enqueue_scripts', 'keystroke_scripts' );


/**
 * Axil admin script
 */
if( !function_exists('keystroke_media_scripts') ) {
    function keystroke_media_scripts() {

        wp_enqueue_style('keystroke-wp-admin', AXIL_ADMIN_CSS_URL . 'admin-style.css', array(), AXIL_VERSION);
        if (is_rtl()){
            wp_enqueue_style('keystroke-rtl-admin', AXIL_ADMIN_CSS_URL . 'rtl-admin.css', array(), AXIL_VERSION);
        }

        // JS
        wp_enqueue_media();
        wp_enqueue_script( 'jquery-ui-tabs' );
        wp_enqueue_script( 'axil-logo-uploader', AXIL_ADMIN_JS_URL .'logo-uploader.js', false, '', true );
    }
}
add_action('admin_enqueue_scripts', 'keystroke_media_scripts', 1000);