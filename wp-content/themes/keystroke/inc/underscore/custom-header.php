<?php
/**
 * Sample implementation of the Custom Header feature
 *
 * You can add an optional custom header image to header.php like so ...
 *
	<?php the_header_image_tag(); ?>
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package keystroke
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses keystroke_header_style()
 */

