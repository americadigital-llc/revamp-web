<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */
if (!class_exists('Redux')) {
    return;
}
$opt_name = AXIL_THEME_FIX . '_options';
$theme = wp_get_theme();
$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name' => $opt_name,
    // This is where your data is stored in the database and also becomes your global variable name.
    'disable_tracking' => true,
    'display_name' => $theme->get('Name'),
    // Name that appears at the top of your panel
    'display_version' => $theme->get('Version'),
    // Version that appears at the top of your panel
    'menu_type' => 'submenu',
    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu' => true,
    // Show the sections below the admin menu item or not
    'menu_title' => esc_html__('Axil Theme Options', 'keystroke'),
    'page_title' => esc_html__('Axil Theme Options', 'keystroke'),
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    //'google_api_key'       => 'AIzaSyC2GwbfJvi-WnYpScCPBGIUyFZF97LI0xs',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography' => false,
    // Use a asynchronous font on the front end or font string
    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
    'admin_bar' => true,
    // Show the panel pages on the admin bar
    'admin_bar_icon' => 'dashicons-menu',
    // Choose an icon for the admin bar menu
    'admin_bar_priority' => 50,
    // Choose an priority for the admin bar menu
    'global_variable' => '',
    // Set a different name for your global variable other than the opt_name
    'dev_mode' => false,
    'forced_dev_mode_off' => false,
    // Show the time the page took to load, etc
    'update_notice' => false,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer' => false,
    // Enable basic customizer support
    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

    // OPTIONAL -> Give you extra features
    'page_priority' => null,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent' => 'themes.php',
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions' => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon' => '',
    // Specify a custom URL to an icon
    'last_tab' => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon' => 'icon-themes',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug' => AXIL_THEME_FIX . '_options',
    // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
    'save_defaults' => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show' => true,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark' => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export' => true,
    // Shows the Import/Export panel when not used as a field.

    // CAREFUL -> These options are for advanced use only
    'transient_time' => 60 * MINUTE_IN_SECONDS,
    'output' => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag' => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    'footer_credit' => '&nbsp;',
    // Disable the footer credit of Redux. Please leave if you can help it.

    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database' => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
    'use_cdn' => true,
    // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.
    'hide_expand' => true,
    // This variable determines if the ‘Expand Options’ buttons is visible on the options panel.
);

Redux::setArgs($opt_name, $args);
/*
 * ---> END ARGUMENTS
 */

// -> START Basic Fields

/**
 * General
 */
Redux::setSection($opt_name, array(
    'title' => esc_html__('General', 'keystroke'),
    'id' => 'axil_general',
    'icon' => 'el el-cog',
));
Redux::setSection($opt_name, array(
    'title' => esc_html__('General Setting', 'keystroke'),
    'id' => 'axil-general-setting',
    'icon' => 'el el-adjust-alt',
    'subsection' => true,
    'fields' => array(
        // Start logo
        array(
            'id' => 'axil_logo_type',
            'type' => 'button_set',
            'title' => esc_html__('Select Logo Type', 'keystroke'),
            'subtitle' => esc_html__('Select logo type, if the image is chosen the existing options of  image below will work, or text option will work. (Note: Used when Transparent Header is enabled.)', 'keystroke'),
            'options' => array(
                'image' => 'Image',
                'text' => 'Text',
            ),
            'default' => 'image',
        ),
        array(
            'id' => 'axil_head_logo',
            'title' => esc_html__('Default Logo', 'keystroke'),
            'subtitle' => esc_html__('Upload the main logo of your site. ( Recommended size: Width 270px and Height: 60px )', 'keystroke'),
            'type' => 'media',
            'default' => array(
                'url' => AXIL_IMG_URL . 'logo.png'
            ),
            'required' => array('axil_logo_type', 'equals', 'image'),
        ),
        array(
            'id' => 'axil_head_logo_white',
            'title' => esc_html__('White Logo', 'keystroke'),
            'subtitle' => esc_html__('Upload the white logo of your site that use into off canvas area. ( Recommended size: Width 270px and Height: 60px )', 'keystroke'),
            'type' => 'media',
            'default' => array(
                'url' => AXIL_IMG_URL . 'white-logo.png'
            ),
            'required' => array('axil_logo_type', 'equals', 'image'),
        ),
        array(
            'id' => 'axil_mobile_logo',
            'title' => esc_html__('Small Off canvas Logo', 'keystroke'),
            'subtitle' => esc_html__('Upload the logo of your site that use into small off canvas area. ( Recommended size: Width 270px and Height: 60px ) ', 'keystroke'),
            'type' => 'media',
            'default' => array(
                'url' => AXIL_IMG_URL . 'mobile-logo.png'
            ),
            'required' => array('axil_logo_type', 'equals', 'image'),
        ),
        array(
            'id' => 'axil_logo_max_height',
            'type' => 'dimensions',
            'units_extended' => true,
            'units' => array('rem', 'px', '%'),
            'title' => esc_html__('Logo Height', 'keystroke'),
            'subtitle' => esc_html__('Set custom logo height. Default value: 50px', 'keystroke'),
            'width' => false,
            'output' => array(
                'max-height' => '.logo img'
            ),
            'required' => array('axil_logo_type', 'equals', 'image'),
        ),
        array(
            'id' => 'axil_logo_padding',
            'type' => 'spacing',
            'title' => esc_html__('Logo Padding', 'keystroke'),
            'subtitle' => esc_html__('Controls the top, right, bottom and left padding of the logo. (Note: Used when Transparent Header is enabled.)', 'keystroke'),
            'mode' => 'padding',
            'units' => array('em', 'px'),
            'default' => array(
                'padding-top' => 'px',
                'padding-right' => 'px',
                'padding-bottom' => 'px',
                'padding-left' => 'px',
                'units' => 'px',
            ),
            'output'         => array('.logo a'),
            'required' => array('axil_logo_type', 'equals', 'image'),
        ),
        array(
            'id' => 'axil_logo_text',
            'type' => 'text',
            'required' => array('axil_logo_type', 'equals', 'text'),
            'title' => esc_html__('Site Title', 'keystroke'),
            'subtitle' => esc_html__('Enter your site title here. (Note: Used when Transparent Header is enabled.)', 'keystroke'),
            'default' => get_bloginfo('name')
        ),
        array(
            'id' => 'axil_logo_text_font',
            'type' => 'typography',
            'title' => esc_html__('Site Title Font Settings', 'keystroke'),
            'required' => array('axil_logo_type', 'equals', 'text'),
            'google' => true,
            'subsets' => false,
            'line-height' => false,
            'text-transform' => true,
            'transition' => false,
            'text-align' => false,
            'preview' => false,
            'all_styles' => true,
            'output' => array('.logo a, .haeder-default .logo a'),
            'units' => 'px',
            'subtitle' => esc_html__('Controls the font settings of the site title. (Note: Used when Transparent Header is enabled.)', 'keystroke'),
            'default' => array(
                'google' => true,
            )
        ),
        // End logo
        array(
            'id' => 'axil_scroll_to_top_enable',
            'type' => 'button_set',
            'title' => esc_html__('Enable Back To Top', 'keystroke'),
            'subtitle' => esc_html__('Enable the back to top button that appears in the bottom right corner of the screen.', 'keystroke'),
            'options' => array(
                'yes' => esc_html__('Yes', 'keystroke'),
                'no' => esc_html__('No', 'keystroke'),
            ),
            'default' => 'yes'
        ),
        array(
            'id' => 'axil_preloader',
            'type' => 'button_set',
            'title' => esc_html__('Enable Preloader', 'keystroke'),
            'options' => array(
                'yes' => esc_html__('Yes', 'keystroke'),
                'no' => esc_html__('No', 'keystroke'),
            ),
            'default' => 'no'
        ),
        array(
            'id' => 'axil_preloader_image',
            'type' => 'media',
            'title' => esc_html__('Preloader Image', 'keystroke'),
            'subtitle' => esc_html__('Please upload your choice of preloader image. Transparent GIF format is recommended', 'keystroke'),
            'default' => array(
                'url' => AXIL_THEME_URI . '/assets/images/preloader.gif'
            ),
            'required' => array('axil_preloader', 'equals', 'yes')
        ),
    )
));


Redux::setSection($opt_name,
    array(
        'title' => esc_html__('Contact & Socials', 'keystroke'),
        'id' => 'socials_section',
        'heading' => esc_html__('Contact & Socials', 'keystroke'),
        'subtitle' => esc_html__('In case you want to hide any field, just keep that field empty', 'keystroke'),
        'icon' => 'el el-twitter',
        'subsection' => true,
        'fields' => array(
            array(
                'id' => 'address_field_title',
                'type' => 'text',
                'title' => esc_html__('Address Field Title', 'keystroke'),
                'default' => esc_html__('Contact information', 'keystroke'),
            ),
            array(
                'id' => 'address',
                'type' => 'textarea',
                'title' => esc_html__('Address', 'keystroke'),
                'default' => esc_html__('Theodore Lowe, Ap #867-859 Sit Rd, Azusa New York', 'keystroke'),
            ),
            array(
                'id' => 'call_now_field_title',
                'type' => 'text',
                'title' => esc_html__('Call Now Field Title', 'keystroke'),
                'default' => esc_html__('We are available 24/ 7. Call Now.', 'keystroke'),
            ),
            array(
                'id' => 'phone',
                'type' => 'text',
                'title' => esc_html__('Phone', 'keystroke'),
                'default' => '(888) 456-2790',
            ),
            array(
                'id' => 'fax',
                'type' => 'text',
                'title' => esc_html__('Fax', 'keystroke'),
                'default' => '(121) 255-53333',
            ),
            array(
                'id' => 'email',
                'type' => 'text',
                'title' => esc_html__('Email', 'keystroke'),
                'validate' => 'email',
                'default' => 'example@domain.com',
            ),
            array(
                'id' => 'social_title',
                'type' => 'text',
                'title' => esc_html__('Social Title', 'keystroke'),
                'default' => esc_html__('Follow us', 'keystroke'),
            ),

            array(
                'id' => 'axil_social_icons',
                'type' => 'sortable',
                'title' => esc_html__('Social Icons', 'keystroke'),
                'subtitle' => esc_html__('Enter social links to show the icons', 'keystroke'),
                'mode' => 'text',
                'label' => true,
                'options' => array(
                    'facebook-f' => '',
                    'twitter' => '',
                    'pinterest-p' => '',
                    'linkedin-in' => '',
                    'instagram' => '',
                    'vimeo-v' => '',
                    'dribbble' => '',
                    'behance' => '',
                    'youtube' => '',
                ),
                'default' => array(
                    'facebook-f' => 'https://www.facebook.com/',
                    'twitter' => 'https://twitter.com/',
                    'pinterest-p' => 'https://pinterest.com/',
                    'linkedin-in' => 'https://linkedin.com/',
                    'instagram' => 'https://instagram.com/',
                    'vimeo-v' => 'https://vimeo.com/',
                    'dribbble' => 'https://dribbble.com/',
                    'behance' => 'https://behance.com/',
                    'youtube' => '',
                ),
            )
        )
    )
);



/**
 * Header Panel
 */
// Redux::setSection( $opt_name, array(
//     'title'            => esc_html__( 'Header', 'keystroke'' ),
//     'id'               => 'axil_header_panel',
//     'icon'             => 'el el-photo',
// ));
/**
 * Header
 */
Redux::setSection($opt_name, array(
        'title' => esc_html__('Header', 'keystroke'),
        'id' => 'header_id',
        'icon' => 'el el-minus',
        'fields' => array(
            array(
                'id' => 'axil_enable_header',
                'type' => 'switch',
                'title' => esc_html__('Header', 'keystroke'),
                'subtitle' => esc_html__('Enable or disable the header area.', 'keystroke'),
                'default' => true
            ),
            // Header Custom Style
            array(
                'id' => 'axil_select_header_template',
                'type' => 'image_select',
                'title' => esc_html__('Select Header Layout', 'keystroke'),
                'options' => array(
                    '1' => array(
                        'alt' => esc_html__('Header Layout 1', 'keystroke'),
                        'title' => esc_html__('Header Layout 1', 'keystroke'),
                        'img' => get_template_directory_uri() . '/assets/images/optionframework/header/1.png',
                    ),
                    '2' => array(
                        'alt' => esc_html__('Header Layout 2', 'keystroke'),
                        'title' => esc_html__('Header Layout 2', 'keystroke'),
                        'img' => get_template_directory_uri() . '/assets/images/optionframework/header/2.png',
                    ),
                    '3' => array(
                        'alt' => esc_html__('Header Layout 3', 'keystroke'),
                        'title' => esc_html__('Header Layout 3', 'keystroke'),
                        'img' => get_template_directory_uri() . '/assets/images/optionframework/header/3.png',
                    ),
                    '4' => array(
                        'alt' => esc_html__('Header Layout 4', 'keystroke'),
                        'title' => esc_html__('Header Layout 4', 'keystroke'),
                        'img' => get_template_directory_uri() . '/assets/images/optionframework/header/4.png',
                    ),
                ),
                'default' => '1',
                'required' => array('axil_enable_header', 'equals', true),
            ),

            array(
                'id' => 'axil_header_btn',
                'type' => 'switch',
                'title' => esc_html__('Header Button', 'keystroke'),
                'on' => esc_html__('Enabled', 'keystroke'),
                'off' => esc_html__('Disabled', 'keystroke'),
                'default' => true,
                'required' => array( 'axil_select_header_template', 'equals', array('4') ),
            ),
            array(
                'id' => 'axil_header_buttontext',
                'type' => 'text',
                'title' => esc_html__('Header button Text', 'keystroke'),
                'default' => esc_html__('Let\'s Talk', 'keystroke'),
                'required' => array('axil_header_btn', 'equals', true),
            ),
            array(
                'id' => 'axil_header_buttonUrl',
                'type' => 'text',
                'default' => '#',
                'title' => esc_html__('Button Url', 'keystroke'),
                'required' => array('axil_header_btn', 'equals', true),

            ),
            array(
                'id' => 'axil_enable_header_offcanvas_menu_icon',
                'type' => 'switch',
                'title' => esc_html__('Offcanvas Menu Icon', 'keystroke'),
                'subtitle' => esc_html__('Enable or disable offcanvas menu icon.', 'keystroke'),
                'default' => false,
                'required' => array( 'axil_select_header_template', 'equals', array('1', '2', '3') ),
            ),
            array(
                'id' => 'axil_header_sticky',
                'type' => 'switch',
                'title' => esc_html__('Header Sticky', 'keystroke'),
                'subtitle' => esc_html__('Enable to activate the sticky header.', 'keystroke'),
                'default' => false,
                'required' => array('axil_enable_header', 'equals', true),
            ),
            array(
                'id' => 'axil_header_transparent',
                'type' => 'switch',
                'title' => esc_html__('Header Transparent', 'keystroke'),
                'subtitle' => esc_html__('Enable to make the header area transparent.', 'keystroke'),
                'default' => true,
                'required' => array('axil_enable_header', 'equals', true),
            ), // output body class


        )
    )
);

//footer Top
Redux::setSection($opt_name, array(
    'title' => esc_html__('Footer Top', 'keystroke'),
    'id' => 'axil_footer_top_section',
    'icon' => 'el el-credit-card',
    'fields' => array(
        array(
            'id' => 'axil_footer_top_enable',
            'type' => 'switch',
            'title' => esc_html__('Footer Top', 'keystroke'),
            'subtitle' => esc_html__('Enable or disable the footer top area.', 'keystroke'),
            'default' => false,
        ),
        // Header Custom Style
        array(
            'id' => 'axil_ft_before_title',
            'type' => 'text',
            'title' => esc_html__('Tag Line', 'keystroke'),
            'default' => 'Lets work together',
            'required' => array('axil_footer_top_enable', 'equals', true),
        ),
        array(
            'id' => 'axil_ft_title',
            'type' => 'text',
            'title' => esc_html__('Title', 'keystroke'),
            'default' => 'Need a successful project?',
            'required' => array('axil_footer_top_enable', 'equals', true),
        ),
        array(
            'id' => 'axil_ft_button_text',
            'type' => 'text',
            'title' => esc_html__('Button Text', 'keystroke'),
            'default' => 'Estimate Project',
            'required' => array('axil_footer_top_enable', 'equals', true),
        ),
        array(
            'id'       => 'axil_ft_button_link_type',
            'type'     => 'select',
            'title'    => esc_html__('Select type', 'keystroke'),
            // Must provide key => value pairs for select options
            'options'  => array(
                '1' => 'Custom Link',
                '2' => 'Internal Page',
            ),
            'default'  => '1',
            'required' => array('axil_footer_top_enable', 'equals', true),
        ),
        array(
            'id' => 'axil_ft_button_link',
            'type' => 'text',
            'title' => esc_html__('Button custom link', 'keystroke'),
            'default' => '#',
            'required' => array('axil_ft_button_link_type', 'equals', '1'),
        ),
        array(
            'id'       => 'axil_ft_button_page_link',
            'type'     => 'select',
            'title'    => esc_html__('Select page', 'keystroke'),
            'data' => 'pages',
            'default'  => '1',
            'required' => array('axil_ft_button_link_type', 'equals', '2'),
        ),
        array(
            'id' => 'axil_ft_button_below_content',
            'type' => 'textarea',
            'title' => esc_html__('Add Content CTA Button Below content', 'keystroke'),
            'default' => '<span class="text wow" data-splitting>Or call us now</span>
                                        <span class="wow" data-splitting><i class="fal fa-phone-alt"></i> <a href="#">(123)
                                    456 7890</a></span>',
            'required' => array('axil_footer_top_enable', 'equals', true),
        ),

    )
));


//footer section
Redux::setSection($opt_name, array(
    'title' => esc_html__('Footer', 'keystroke'),
    'id' => 'axil_footer_section',
    'icon' => 'el el-photo',
    'fields' => array(
        array(
            'id' => 'axil_footer_enable',
            'type' => 'switch',
            'title' => esc_html__('Footer', 'keystroke'),
            'subtitle' => esc_html__('Enable or disable the footer area.', 'keystroke'),
            'default' => true,
        ),
        // Header Custom Style
        array(
            'id' => 'axil_select_footer_template',
            'type' => 'image_select',
            'title' => esc_html__('Select Footer Layout', 'keystroke'),
            'options' => array(
                '1' => array(
                    'alt' => esc_html__('Footer Layout 1', 'keystroke'),
                    'title' => esc_html__('Footer Layout 1', 'keystroke'),
                    'img' => get_template_directory_uri() . '/assets/images/optionframework/footer/1.png',
                ),
                '2' => array(
                    'alt' => esc_html__('Footer Layout 2', 'keystroke'),
                    'title' => esc_html__('Footer Layout 2', 'keystroke'),
                    'img' => get_template_directory_uri() . '/assets/images/optionframework/footer/2.png',
                ),
                '3' => array(
                    'alt' => esc_html__('Footer Layout 3', 'keystroke'),
                    'title' => esc_html__('Footer Layout 3', 'keystroke'),
                    'img' => get_template_directory_uri() . '/assets/images/optionframework/footer/3.png',
                ),
                '4' => array(
                    'alt' => esc_html__('Footer Layout 4', 'keystroke'),
                    'title' => esc_html__('Footer Layout 4', 'keystroke'),
                    'img' => get_template_directory_uri() . '/assets/images/optionframework/footer/4.png',
                ),
            ),
            'default' => '2',
            'required' => array('axil_footer_enable', 'equals', true),
        ),
        array(
            'id' => 'axil_footer_social_network',
            'type' => 'switch',
            'title' => esc_html__('Footer Social Network', 'keystroke'),
            'subtitle' => esc_html__('Enable or disable the footer social network.', 'keystroke'),
            'default' => false,
            'required' => array('axil_select_footer_template', 'equals', '1'),
        ),
        // Footer Bottom
        array(
            'id' => 'axil_copyright_contact',
            'type' => 'editor',
            'title' => esc_html__('Copyright Content', 'keystroke'),
            'args' => array(
                'teeny' => true,
                'textarea_rows' => 5,
            ),
            'default' => '© 2020. All rights reserved by <a href="#" target="_blank" rel="noopener">Your Company.</a>',
            'required' => array('axil_footer_enable', 'equals', true),
        ),

    )
));


//  Page Banner/Title section
Redux::setSection($opt_name, array(
    'title' => esc_html__('Page Banner', 'keystroke'),
    'id' => 'axil_banner_section',
    'icon' => 'el el-website',
    'fields' => array(
        array(
            'id' => 'axil_banner_enable',
            'type' => 'switch',
            'title' => esc_html__('Banner', 'keystroke'),
            'subtitle' => esc_html__('Enable or disable the banner area.', 'keystroke'),
            'default' => true,
        ),
        // Header Custom Style
        array(
            'id' => 'axil_select_banner_template',
            'type' => 'image_select',
            'title' => esc_html__('Select banner Layout', 'keystroke'),
            'options' => array(
                '1' => array(
                    'alt' => esc_html__('Banner Layout 1', 'keystroke'),
                    'title' => esc_html__('banner Layout 1', 'keystroke'),
                    'img' => get_template_directory_uri() . '/assets/images/optionframework/banner/1.jpg',
                ),
                '2' => array(
                    'alt' => esc_html__('Banner Layout 2', 'keystroke'),
                    'title' => esc_html__('banner Layout 2', 'keystroke'),
                    'img' => get_template_directory_uri() . '/assets/images/optionframework/banner/2.jpg',
                ),
            ),
            'default' => '1',
            'required' => array('axil_banner_enable', 'equals', true),
        ),
        array(
            'id' => 'axil_breadcrumb_enable',
            'type' => 'switch',
            'title' => esc_html__('Breadcrumb', 'keystroke'),
            'subtitle' => esc_html__('Enable or disable the breadcrumb area.', 'keystroke'),
            'default' => true,
            'required' => array('axil_select_banner_template', 'equals', '1'),

        ),
    )
));

/**
 * Blog Panel
 */
Redux::setSection($opt_name, array(
    'title' => esc_html__('Blog', 'keystroke'),
    'id' => 'axil_blog',
    'icon' => 'el el-file-edit',
));

// Blog Options
Redux::setSection($opt_name, array(
        'title' => esc_html__('Archive', 'keystroke'),
        'id' => 'axil_blog_genaral',
        'icon' => 'el el-edit',
        'subsection' => true,
        'fields' => array(
            array(
                'id' => 'axil_enable_blog_title',
                'type' => 'button_set',
                'title' => esc_html__('Title', 'keystroke'),
                'subtitle' => esc_html__('Enable or Disable the blog page title.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Enable', 'keystroke'),
                    'no' => esc_html__('Disable', 'keystroke'),
                ),
                'default' => 'yes',
            ),
            array(
                'id' => 'axil_blog_text',
                'type' => 'text',
                'title' => esc_html__('Default Title', 'keystroke'),
                'subtitle' => esc_html__('Controls the Default title of the page which is displayed on the page title are on the blog page.', 'keystroke'),
                'default' => esc_html__('Blogs', 'keystroke'),
                'required' => array('axil_enable_blog_title', 'equals', 'yes'),
            ),
            array(
                'id' => 'axil_blog_sidebar',
                'type' => 'image_select',
                'title' => esc_html__('Select Blog Sidebar', 'keystroke'),
                'subtitle' => esc_html__('Choose your favorite blog layout', 'keystroke'),
                'options' => array(
                    'left' => array(
                        'alt' => esc_html__('Left Sidebar', 'keystroke'),
                        'img' => get_template_directory_uri() . '/assets/images/optionframework/layout/left-sidebar.png',
                        'title' => esc_html__('Left Sidebar', 'keystroke'),
                    ),
                    'right' => array(
                        'alt' => esc_html__('Right Sidebar', 'keystroke'),
                        'img' => get_template_directory_uri() . '/assets/images/optionframework/layout/right-sidebar.png',
                        'title' => esc_html__('Right Sidebar', 'keystroke'),
                    ),
                    'no' => array(
                        'alt' => esc_html__('No Sidebar', 'keystroke'),
                        'img' => get_template_directory_uri() . '/assets/images/optionframework/layout/no-sidebar.png',
                        'title' => esc_html__('No Sidebar', 'keystroke'),
                    ),
                ),
                'default' => 'right',
            ),
            array(
                'id' => 'axil_show_post_author_meta',
                'type' => 'button_set',
                'title' => esc_html__('Author', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the author of blog post.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'yes',
            ),
            array(
                'id' => 'axil_show_post_publish_date_meta',
                'type' => 'button_set',
                'title' => esc_html__('Publish Date', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the publish date of blog post.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'yes',
            ),
            array(
                'id' => 'axil_show_post_updated_date_meta',
                'type' => 'button_set',
                'title' => esc_html__('Updated Date', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the updated date of blog post.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'no',
            ),
            array(
                'id' => 'axil_show_post_reading_time_meta',
                'type' => 'button_set',
                'title' => esc_html__('Reading Time', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the publish content reading time.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'yes',
            ),
            array(
                'id' => 'axil_show_post_comments_meta',
                'type' => 'button_set',
                'title' => esc_html__('Comments', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the comments of blog post.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'no',
            ),
            array(
                'id' => 'axil_show_post_categories_meta',
                'type' => 'button_set',
                'title' => esc_html__('Categories', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the categories of blog post.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'yes',
            ),
            array(
                'id' => 'axil_show_post_tags_meta',
                'type' => 'button_set',
                'title' => esc_html__('Tags', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the tags of blog post.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'yes',
            ),
            array(
                'id' => 'axil_enable_readmore_btn',
                'type' => 'button_set',
                'title' => esc_html__('Read More Button', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the read more button of blog post.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'yes',
            ),
            array(
                'id' => 'axil_readmore_text',
                'type' => 'text',
                'title' => esc_html__('Read More Text', 'keystroke'),
                'subtitle' => esc_html__('Set the Default title of read more button.', 'keystroke'),
                'default' => esc_html__('Read More', 'keystroke'),
                'required' => array('axil_enable_readmore_btn', 'equals', 'yes'),
            ),
        )
    )
);

/**
 * Single Post
 */
Redux::setSection($opt_name, array(
        'title' => esc_html__('Single', 'keystroke'),
        'id' => 'axil_blog_details_id',
        'icon' => 'el el-website',
        'subsection' => true,
        'fields' => array(
            array(
                'id' => 'axil_enable_single_post_breadcrumb_wrap',
                'type' => 'button_set',
                'title' => esc_html__('Breadcrumb Area', 'keystroke'),
                'subtitle' => esc_html__('Enable or Disable Breadcrumb Area.', 'keystroke'),
                'options' => array(
                    'show' => esc_html__('Enable', 'keystroke'),
                    'hide' => esc_html__('Disable', 'keystroke'),
                ),
                'default' => 'show',
            ),
            array(
                'id' => 'axil_single_pos',
                'type' => 'image_select',
                'title' => esc_html__('Blog Details Layout', 'keystroke'),
                'subtitle' => esc_html__('Choose your favorite layout', 'keystroke'),
                'options' => array(
                    'left' => array(
                        'alt' => esc_html__('Left Sidebar', 'keystroke'),
                        'img' => get_template_directory_uri() . '/assets/images/optionframework/layout/left-sidebar.png',
                        'title' => esc_html__('Left Sidebar', 'keystroke'),
                    ),
                    'right' => array(
                        'alt' => esc_html__('Right Sidebar', 'keystroke'),
                        'img' => get_template_directory_uri() . '/assets/images/optionframework/layout/right-sidebar.png',
                        'title' => esc_html__('Right Sidebar', 'keystroke'),
                    ),
                    'full' => array(
                        'alt' => esc_html__('Full Width', 'keystroke'),
                        'img' => get_template_directory_uri() . '/assets/images/optionframework/layout/no-sidebar.png',
                        'title' => esc_html__('Full Width', 'keystroke'),
                    ),
                ),
                'default' => 'full',
            ),
            array(
                'id' => 'axil_show_blog_details_author_meta',
                'type' => 'button_set',
                'title' => esc_html__('Author', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the author of blog post.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'yes',
            ),
            array(
                'id' => 'axil_show_blog_details_publish_date_meta',
                'type' => 'button_set',
                'title' => esc_html__('Publish Date', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the publish date of blog post.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'yes',
            ),
            array(
                'id' => 'axil_show_blog_details_updated_date_meta',
                'type' => 'button_set',
                'title' => esc_html__('Updated Date', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the updated date of blog post.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'no',
            ),
            array(
                'id' => 'axil_show_blog_details_reading_time_meta',
                'type' => 'button_set',
                'title' => esc_html__('Reading Time', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the publish content reading time.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'yes',
            ),
            array(
                'id' => 'axil_show_blog_details_comments_meta',
                'type' => 'button_set',
                'title' => esc_html__('Comments', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the comments of blog post.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'no',
            ),
            array(
                'id' => 'axil_show_blog_details_categories_meta',
                'type' => 'button_set',
                'title' => esc_html__('Categories', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the categories of blog post.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'yes',
            ),
            array(
                'id' => 'axil_show_blog_details_tags_meta',
                'type' => 'button_set',
                'title' => esc_html__('Tags', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the tags of blog post.', 'keystroke'),
                'options' => array(
                    'yes' => esc_html__('Show', 'keystroke'),
                    'no' => esc_html__('Hide', 'keystroke'),
                ),
                'default' => 'yes',
            ),

            array(
                'id' => 'axil_blog_details_social_share',
                'type' => 'switch',
                'title' => esc_html__('Social Share', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the social share of single post.', 'keystroke'),
                'default' => false,
            ),
            array(
                'id' => 'axil_blog_details_social_share_label',
                'type' => 'text',
                'title' => esc_html__('Social Share Label', 'keystroke'),
                'subtitle' => esc_html__('Enter your social share label hare.', 'keystroke'),
                'default' => esc_html__('Share on:', 'keystroke'),
                'required' => array('axil_blog_details_social_share', 'equals', true),
            ),
            array(
                'id' => 'axil_blog_details_show_author_info',
                'type' => 'switch',
                'title' => esc_html__('Show Author Info', 'keystroke'),
                'subtitle' => esc_html__('Show or hide the Author Info box of single post.', 'keystroke'),
                'default' => false,
            )
        )
    )
);
// Typography
Redux::setSection($opt_name, array(
        'title' => esc_html__('Typography', 'keystroke'),
        'id' => 'axil_fonts',
        'icon' => 'el el-fontsize',
        'fields' => array(
            array(
                'id' => 'axil_h1_typography',
                'type' => 'typography',
                'title' => esc_html__('H1 Heading Typography (Default size: 56px)', 'keystroke'),
                'subtitle' => esc_html__('Controls the typography settings of the H1 heading.', 'keystroke'),
                'google' => true,
                'text-transform' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'subsets' => false,
                'text-align' => false,
                'all_styles' => true,
                'color' => false,
                'units' => 'px',
                'output' => array('h1, .h1'),
            ),
            array(
                'id' => 'axil_h2_typography',
                'type' => 'typography',
                'title' => esc_html__('H2 Heading Typography (Default size: 50px)', 'keystroke'),
                'subtitle' => esc_html__('Controls the typography settings of the H2 heading.', 'keystroke'),
                'google' => true,
                'text-transform' => true,
                'letter-spacing' => true,
                'word-spacing' => true,
                'subsets' => false,
                'text-align' => false,
                'all_styles' => true,
                'color' => false,
                'units' => 'px',
                'output' => array('h2, .h2'),
            ),
            array(
                'id' => 'axil_h3_typography',
                'type' => 'typography',
                'title' => esc_html__('H3 Heading Typography (Default size: 35px)', 'keystroke'),
                'subtitle' => esc_html__('Controls the typography settings of the H3 heading.', 'keystroke'),
                'google' => true,
                'text-transform' => true,
                'letter-spacing' => true,
                'word-spacing' => true,
                'subsets' => false,
                'text-align' => false,
                'all_styles' => true,
                'color' => false,
                'units' => 'px',
                'output' => array('h3, .h3'),
            ),
            array(
                'id' => 'axil_h4_typography',
                'type' => 'typography',
                'title' => esc_html__('H4 Heading Typography (Default size: 26px)', 'keystroke'),
                'subtitle' => esc_html__('Controls the typography settings of the H4 heading.', 'keystroke'),
                'google' => true,
                'text-transform' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'subsets' => false,
                'text-align' => false,
                'all_styles' => true,
                'color' => false,
                'units' => 'px',
                'output' => array('h4, .h4'),
            ),
            array(
                'id' => 'axil_h5_typography',
                'type' => 'typography',
                'title' => esc_html__('H5 Heading Typography (Default size: 22px)', 'keystroke'),
                'subtitle' => esc_html__('Controls the typography settings of the H5 heading.', 'keystroke'),
                'google' => true,
                'text-transform' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'subsets' => false,
                'text-align' => false,
                'all_styles' => true,
                'color' => false,
                'units' => 'px',
                'output' => array('h5, .h5'),
            ),
            array(
                'id' => 'axil_h6_typography',
                'type' => 'typography',
                'title' => esc_html__('H6 Heading Typography (Default size: 16px)', 'keystroke'),
                'subtitle' => esc_html__('Controls the typography settings of the H6 heading.', 'keystroke'),
                'google' => true,
                'text-transform' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'subsets' => false,
                'text-align' => false,
                'all_styles' => true,
                'color' => false,
                'units' => 'px',
                'output' => array('h6, .h6'),
            ),
            array(
                'id' => 'axil_b1_typography',
                'type' => 'typography',
                'title' => esc_html__('B1 Typography (Default size: 24px)', 'keystroke'),
                'subtitle' => esc_html__('B1 is used in subtitle and quote', 'keystroke'),
                'google' => true,
                'subsets' => false,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => false,
                'all_styles' => true,
                'color' => false,
                'output' => array('.subtitle-3, p.subtitle-3'),
                'units' => 'px',
            ),
            array(
                'id' => 'axil_b2_typography',
                'type' => 'typography',
                'title' => esc_html__('B2 Typography (Default size: 20px)', 'keystroke'),
                'subtitle' => esc_html__('B2 is used in subtitle and paragraph', 'keystroke'),
                'google' => true,
                'subsets' => false,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => false,
                'all_styles' => true,
                'color' => false,
                'output' => array('.subtitle-2'),
                'units' => 'px',
            ),
            array(
                'id' => 'axil_b3_typography',
                'type' => 'typography',
                'title' => esc_html__('B3 Typography (Default size: 18px)', 'keystroke'),
                'subtitle' => esc_html__('B3 is used in subtitle, paragraph, footer link and body', 'keystroke'),
                'google' => true,
                'subsets' => false,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => false,
                'all_styles' => true,
                'color' => false,
                'output' => array('body, p'),
                'units' => 'px',
            ),
            array(
                'id' => 'axil_b4_typography',
                'type' => 'typography',
                'title' => esc_html__('B4 Typography (Default size: 15px)', 'keystroke'),
                'subtitle' => esc_html__('B4 is used in blog meta, tab text and footer copyright section', 'keystroke'),
                'google' => true,
                'subsets' => false,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => false,
                'all_styles' => true,
                'color' => false,
                'output' => array('.subtitle-1'),
                'units' => 'px',
            ),

        )
    )
);

//404 error page
Redux::setSection($opt_name, array(
    'title' => esc_html__('404 Page', 'keystroke'),
    'id' => 'axil_error_page',
    'icon' => 'el el-eye-close',
    'fields' => array(
        array(
            'id' => 'axil_404_title',
            'type' => 'text',
            'title' => esc_html__('Title', 'keystroke'),
            'subtitle' => esc_html__('Add your Default title.', 'keystroke'),
            'value' => 'Page not Found',
            'default' => esc_html__('Page not Found', 'keystroke'),
        ),
        array(
            'id' => 'axil_404font',
            'type' => 'typography',
            'title' => esc_html__('Title Typography', 'keystroke'),
            'subtitle' => esc_html__('Controls the typography for the title.', 'keystroke'),
            'google' => true,
            'font-style' => true,
            'font-weight' => true,
            'font-family' => true,
            'subsets' => true,
            'line-height' => true,
            'text-align' => true,
            'all_styles' => true,
            'output' => array('.axil-error-not-found .axil-error .title'),
            'units' => 'px',
            'default' => array(
                'google' => true,
            ),
        ),
        
        array(
            'id' => 'axil_404_subtitle',
            'type' => 'text',
            'title' => esc_html__('Sub Title', 'keystroke'),
            'subtitle' => esc_html__('Add your custom subtitle.', 'keystroke'),
            'default' => esc_html__('Lorem ipsum dolor sit amet consectetur, adipisicing elit. Enim, recusandae
            consectetur nesciunt magnam facilis aliquid amet earum alias?', 'keystroke'),
        ),
        array(
            'id' => 'axil_404font_subtitle',
            'type' => 'typography',
            'title' => esc_html__('Sub Title Typography', 'keystroke'),
            'subtitle' => esc_html__('Controls the typography settings of the subtitle.', 'keystroke'),
            'google' => true,
            'font-backup' => false,
            'subsets' => false,
            'line-height' => false,
            'text-align' => false,
            'text-transform' => true,
            'all_styles' => true,
            'output' => array('.axil-error-not-found .axil-error .subtitle-2'),
            'units' => 'px',
            'default' => array(
                'google' => true,
            )
        ),
        array(
            'id' => 'axil_enable_go_back_btn',
            'type' => 'button_set',
            'title' => esc_html__('Button', 'keystroke'),
            'subtitle' => esc_html__('Enable or disable the go to home page button.', 'keystroke'),
            'options' => array(
                'yes' => 'Enable',
                'no' => 'Disable'
            ),
            'default' => 'yes'
        ),
        array(
            'id' => 'axil_button_text',
            'type' => 'text',
            'title' => esc_html__('Button Text', 'keystroke'),
            'subtitle' => esc_html__('Set the custom text of go to home page button.', 'keystroke'),
            'default' => esc_html__('Go Back', 'keystroke'),
            'required' => array('axil_enable_go_back_btn', 'equals', 'yes'),
        )
    )
));


