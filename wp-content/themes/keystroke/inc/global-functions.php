<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package keystroke
 */

/**
 * Enqueue scripts and styles.
 */
if (!function_exists('keystroke_content_estimated_reading_time')) {
    /**
     * Function that estimates reading time for a given $content.
     * @param string $content Content to calculate read time for.
     * @paramint $wpm Estimated words per minute of reader.
     * @returns int $time Esimated reading time.
     */
    function keystroke_content_estimated_reading_time($content = '', $wpm = 200)
    {
        $clean_content = strip_shortcodes($content);
        $clean_content = strip_tags($clean_content);
        $word_count = str_word_count($clean_content);
        $time = ceil($word_count / $wpm);
        $output = $time . esc_attr__(' min read', 'keystroke');
        return $output;
    }
}


/**
 * Escapeing
 */
if ( !function_exists('awescapeing') ) {
    function awescapeing($html){
        return $html;
    }
}

/**
 *  Convert Get Theme Option global to function
 */
if(!function_exists('axil_get_opt')){
    function axil_get_opt(){
        global $axil_option;
        return $axil_option;
    }
}
/**
 * Get terms
 */
function axil_get_terms_gb( $term_type = null, $hide_empty = false ) {
    if(!isset( $term_type )){
        return;
    }
    $axil_custom_terms = array();
    $terms = get_terms( $term_type, array( 'hide_empty' => $hide_empty ) );
    array_push( $axil_custom_terms, esc_html__( '--- Select ---', 'keystroke' ) );
    if ( is_array( $terms ) && ! empty( $terms ) ) {
        foreach ( $terms as $single_term ) {
            if ( is_object( $single_term ) && isset( $single_term->name, $single_term->slug ) ) {
                $axil_custom_terms[ $single_term->slug ] = $single_term->name;
            }
        }
    }
    return $axil_custom_terms;
}

/**
 * Blog Pagination
 */
if(!function_exists('axil_blog_pagination')){
    function axil_blog_pagination(){
        GLOBAL $wp_query;
        if ($wp_query->post_count < $wp_query->found_posts) {
            ?>
            <div class="post-pagination"> <?php
                the_posts_pagination(array(
                    'prev_text'          => '<i class="fal fa-arrow-left"></i>',
                    'next_text'          => '<i class="fal fa-arrow-right"></i>',
                    'type'               => 'list',
                    'show_all'  	     => false,
                    'end_size'           => 1,
                    'mid_size'           => 8,
                )); ?>
            </div>
            <?php
        }
    }
}

/**
 * Short Title
 */
if (!function_exists('axil_short_title')){
    function axil_short_title($title, $length = 30) {
        if (strlen($title) > $length) {
            return substr($title, 0, $length) . ' ...';
        }else {
            return $title;
        }
    }
}


/**
 * Get ACF data conditionally
 */
if( !function_exists('axil_get_acf_data') ){
    function axil_get_acf_data($fields){
        return (class_exists('ACF') && get_field_object($fields)) ? get_field($fields) : false;
    }

}
