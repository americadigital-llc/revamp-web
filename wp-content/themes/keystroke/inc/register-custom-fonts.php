<?php
/**
 * Register custom fonts.
 */
if ( !function_exists( 'axil_fonts_url' ) ) :
    function axil_fonts_url() {
        $fonts_url = '';
        $fonts     = array();
        $subsets   = 'latin,latin-ext';

        /* translators: If there are characters in your language that are not supported by Nunito+Sans Sans, translate this to 'off'. Do not translate into your own language. */
        if ( 'off' !== esc_attr_x( 'on', 'DM Sans font: on or off', 'keystroke' ) ) {
            $fonts[] = 'DM Sans:400,400i,500,500i,700';
        }

        if ( $fonts ) {
            $fonts_url = add_query_arg( array(
                'family' => urlencode( implode( '|', $fonts ) ),
                'subset' => urlencode( $subsets ),
            ), 'https://fonts.googleapis.com/css' );
        }

        return esc_url_raw($fonts_url);
    }
endif;

