<?php
/**
 * The template for displaying all single project/portfolio
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package keystroke
 */
get_header();
while ( have_posts() ) :
    the_post();

    the_content();

endwhile; // End of the loop.
get_footer();
