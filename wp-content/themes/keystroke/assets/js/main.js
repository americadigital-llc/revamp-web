/*********************************************************************************
	Template Name: Keystroke Creative Agency WordPress Theme
	Note: This is main js.

**********************************************************************************/
(function (window, document, $, undefined) {
    'use strict';

    var axilKey = {
        k: function (e) {
			axilKey.s();
			axilKey.methods();
        },

        s: function (e) {
			this._window = $(window),
			this._document = $(document),
			this._body = $('body'),
            this._html = $('html'),
            this.sideNav = $('.side-nav'),
            this._navsearch = $('.axil-search-area')
        }, 

        methods: function (e) {
            axilKey.axilWow();
            axilKey.counterUp();
            axilKey.ytPlayer();
            axilKey.scrollTop();
            axilKey.axilHover();
            axilKey.axilaccor();
            axilKey.marqueImages();
            axilKey.stickHeader();
            axilKey.mobileMenu();
            axilKey.mobileMenu2();
            axilKey.scrollSmoth();
            axilKey.galleryPostActivation();
            axilKey._clickDoc();
            axilKey._disable_load_more_button();
            axilKey.checkVal();
            axilKey.accordionClassInit();
            axilKey.preloaderInit();
        },

        checkVal: function () {
            $('input, textarea, select').each(function() {
                if($(this).val()){
                    $(this).parents('.form-group').addClass('focused');
                }
            });
        },
        
        accordionClassInit: function(){
            var accoddionWrapper = $('.axil-accordion--2 .collapse');
            var accoddionWrapperShow = $('.axil-accordion--2 .collapse.show');
            accoddionWrapperShow.parent().addClass('card-active');
            accoddionWrapper.on('shown.bs.collapse', function () {
                $(this).parent().addClass('card-active');
            });
            accoddionWrapper.on('hidden.bs.collapse', function () {
                $(this).parent().removeClass('card-active');
            });
        },

        preloaderInit: function(){
            axilKey._window.on('load', function () {
                $('#preloader').fadeOut('slow', function () {
                    $(this).remove();
                });
            });
        },

        axilWow: function () {
            new WOW().init();
        },

        galleryPostActivation: function () {
            $('.axil-gallery-post').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                adaptiveHeight: true,
                arrows: true,
                prevArrow: '<button class="slick-btn slick-prev"><i class="fal fa-angle-left"></i></button>',
                nextArrow: '<button class="slick-btn slick-next"><i class="fal fa-angle-right"></i></button>'
            });
        },

        mobileMenu: function () {
            $('.mainmenu-item > li.has-children > a , .mainmenu-item > li.has-children > .submenu li.has-children > a').on('click', function (e) {
                e.preventDefault();
                $(this).siblings('.submenu').slideToggle('400');
                $(this).toggleClass('active').siblings('.submenu').toggleClass('is-visiable');
            })
        },

        stickHeader: function () {
            axilKey._window.scroll(function () {
                if ($(this).scrollTop() > 250) {
                    $('.axil-header-sticky').addClass('sticky');
                    $('body').addClass('header-sticky-now');
                }else{
                    $('.axil-header-sticky').removeClass('sticky');
                    $('body').removeClass('header-sticky-now');
                }
            })
        },

        scrollSmoth: function (e) {
            $(document).on('click', '.smoth-animation', function (event) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: $($.attr(this, 'href')).offset().top
                }, 500);
            });
        },

        marqueImages: function () {
            $('.marque-images').each(function () {
                var t = 0;
                var i = 1;
                var $this = $(this);
                setInterval(function () {
                    t += i;
                    $this.css('background-position-x', -t + "px");
                }, 10);
            });
        },

        counterUp: function () {
			var _counter = $('.count');
			if (_counter.length) {
				_counter.counterUp({
					delay: 10,
					time: 1000,
					triggerOnce: true
				});
			}
        },

        mobileMenu2: function () {
            $('.side-nav-content .main-navigation > li.menu-item-has-children > a , .side-nav-content .main-navigation > li.menu-item-has-children > .sub-menu li.menu-item-has-children > a').on('click', function (e) {
                e.preventDefault();
                $(this).siblings('.sub-menu').slideToggle('400');
                $(this).toggleClass('active').siblings('.sub-menu').toggleClass('is-visiable');
            })
        },

        ytPlayer: function () {
            $('.play__btn').yu2fvl();
        },

        scrollTop: function () {
            if($("body").hasClass("active-scroll-to-top")){
                $.scrollUp({
                    scrollText: '<span class="text">top</span>',
                    easingType: 'linear',
                    scrollSpeed: 900,
                    animation: 'slide'
                });
            }
        },

        axilHover : function () {
            $('body').on('mouseenter','.mesonry-list .portfolio , .blog-list-wrapper .axil-blog , .axil-testimonial-single .axil-testimonial',function(){
                var self=this;
                $(self).removeClass('axil-control');
                setTimeout(function() {
                    $('.axil-service-area .active , .mesonry-list .active , .blog-list-wrapper .active , .axil-testimonial-single .active').removeClass('active').addClass('axil-control');
                    $(self).removeClass('axil-control').addClass('active');
                    $('.axil-service.active .inner::before').css("opacity", 0.1);
                }, 0);
            });
        },

        axilaccor: function () {
            $('.axil-accordion .card .card-header').siblings('.collapse.show').parent().addClass('open');
        },

        _slickDoc: function (e) {
            // Check if element exists
            $.fn.elExists = function () {
                return this.length > 0;
            };
            // Variables
            var $html = $('html'),
                $elementCarousel = $('.axil-carousel');
    
            if ($elementCarousel.elExists()) {
                var slickInstances = [];
                $elementCarousel.each(function (index, element) {
                    var $this = $(this);
                    // Carousel Options
                    var $options = typeof $this.data('slick-options') !== 'undefined' ? $this.data('slick-options') : '';
                    var $spaceBetween = $options.spaceBetween ? parseInt($options.spaceBetween) : 0,
                        $spaceBetween_xl = $options.spaceBetween_xl ? parseInt($options.spaceBetween_xl) : 0,
                        $isCustomArrow = $options.isCustomArrow ? $options.isCustomArrow : false,
                        $customPrev = $isCustomArrow === true ? ($options.customPrev ? $options.customPrev : '') : '',
                        $customNext = $isCustomArrow === true ? ($options.customNext ? $options.customNext : '') : '',
                        $vertical = $options.vertical ? $options.vertical : false,
                        $focusOnSelect = $options.focusOnSelect ? $options.focusOnSelect : false,
                        $asNavFor = $options.asNavFor ? $options.asNavFor : '',
                        $fade = $options.fade ? $options.fade : false,
                        $autoplay = $options.autoplay ? $options.autoplay : false,
                        $autoplaySpeed = $options.autoplaySpeed ? $options.autoplaySpeed : 5000,
                        $swipe = $options.swipe ? $options.swipe : false,
                        $adaptiveHeight = $options.adaptiveHeight ? $options.adaptiveHeight : false,
    
                        $arrows = $options.arrows ? $options.arrows : false,
                        $dots = $options.dots ? $options.dots : false,
                        $infinite = $options.infinite ? $options.infinite : false,
                        $centerMode = $options.centerMode ? $options.centerMode : false,
                        $centerPadding = $options.centerPadding ? $options.centerPadding : '',
                        $speed = $options.speed ? parseInt($options.speed) : 1000,
                        $prevArrow = $arrows === true ? ($options.prevArrow ? '<span class="' + $options.prevArrow.buttonClass + '"><i class="' + $options.prevArrow.iconClass + '"></i></span>' : '<button class="slick-prev">previous</span>') : '',
                        $nextArrow = $arrows === true ? ($options.nextArrow ? '<span class="' + $options.nextArrow.buttonClass + '"><i class="' + $options.nextArrow.iconClass + '"></i></span>' : '<button class="slick-next">next</span>') : '',
                        $slidesToShow = $options.slidesToShow ? parseInt($options.slidesToShow, 10) : 1,
                        $slidesToScroll = $options.slidesToScroll ? parseInt($options.slidesToScroll, 10) : 1;
    
                    /*Responsive Variable, Array & Loops*/
                    var $responsiveSetting = typeof $this.data('slick-responsive') !== 'undefined' ? $this.data('slick-responsive') : '',
                        $responsiveSettingLength = $responsiveSetting.length,
                        $responsiveArray = [];
                    for (var i = 0; i < $responsiveSettingLength; i++) {
                        $responsiveArray[i] = $responsiveSetting[i];
    
                    }
    
                    // Adding Class to instances
                    $this.addClass('slick-carousel-' + index);
                    $this.parent().find('.slick-dots').addClass('dots-' + index);
                    $this.parent().find('.slick-btn').addClass('btn-' + index);
    
                    if ($spaceBetween != 0) {
                        $this.addClass('slick-gutter-' + $spaceBetween);
                    }
                    if ($spaceBetween_xl != 0) {
                        $this.addClass('slick-gutter-xl-' + $spaceBetween_xl);
                    }
                    $this.not('.slick-initialized').slick({
                        slidesToShow: $slidesToShow,
                        slidesToScroll: $slidesToScroll,
                        asNavFor: $asNavFor,
                        autoplay: $autoplay,
                        autoplaySpeed: $autoplaySpeed,
                        speed: $speed,
                        infinite: $infinite,
                        arrows: $arrows,
                        dots: $dots,
                        vertical: $vertical,
                        focusOnSelect: $focusOnSelect,
                        centerMode: $centerMode,
                        centerPadding: $centerPadding,
                        fade: $fade,
                        adaptiveHeight: $adaptiveHeight,
                        prevArrow: $prevArrow,
                        nextArrow: $nextArrow,
                        responsive: $responsiveArray,
                    });
    
                    if ($isCustomArrow === true) {
                        $($customPrev).on('click', function () {
                            $this.slick('slickPrev');
                        });
                        $($customNext).on('click', function () {
                            $this.slick('slickNext');
                        });
                    }
                });
    
                // Updating the sliders in tab
                $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                    $elementCarousel.slick('setPosition');
                });
            }
        },
        
        _clickDoc: function (e) {
            var inputblur, inputFocus, openSideNav, closeSideNav, openSubMenu , closeSubMenu, searchTriggerShow , searchTriggerHide, axilaccordion, OpenMobileMenu, closeMobileMenu, closeMenuWrapperClick;

            inputblur = function (e) {
				if (!$(this).val()) {
                    $(this).parent('span').parent('.form-group').removeClass('focused');
                    $(this).parent('.form-group').removeClass('focused');
				}
            };

            inputFocus = function (e) {
				$(this).parents('.form-group').addClass('focused');
            };

            openSideNav = function (e) {
                e.preventDefault();
                axilKey.sideNav.addClass('opened');
                axilKey._html.addClass('side-nav-opened');
            };

            closeSideNav = function (e) {
				if (!$('.side-nav, .side-nav *:not(".close-sidenav, .close-sidenav *")').is(e.target)) {
					axilKey.sideNav.removeClass('opened');
                    axilKey._html.removeClass('side-nav-opened');
                }
            };

            OpenMobileMenu = function (e) {
                e.preventDefault();
                axilKey._body.addClass('popup-mobile-manu-visible');
                axilKey._html.css({
                    overflow: "hidden"
                })
            };

            closeMenuWrapperClick = function (e) {
                e.target === this && axilKey._body.removeClass('popup-mobile-manu-visible'),
                axilKey._html.css({
                    overflow: ""
                });
            };

            closeMobileMenu = function (e) {
                $('.mainmenu-item > li.has-children > a , .mainmenu-item > li.has-children > .submenu li.has-children > a').removeClass('active').siblings('.submenu').slideUp('400');
                axilKey._body.removeClass('popup-mobile-manu-visible');
                axilKey._html.css({
                    overflow: ""
                })
            };

            openSubMenu = function (e) {
                if (axilKey._window.width() < 992) {
                    $(this).siblings('.axil-submenu').slideToggle('active').parents('li').toggleClass('active')
                }else {
                    $(this).siblings('.axil-submenu').toggleClass('visible').parents('li').toggleClass('active');
                    $(this).parents('li').siblings('.has-dropdown').removeClass('active').find('.axil-submenu').removeClass('visible');
                }
            };

            closeSubMenu = function (e) {
                if (!$('.mainmenu-nav ul.mainmenu li a').is(e.target)) {
                    $('.axil-submenu').removeClass('visible').parents('li').removeClass('active');
                }
            };

            searchTriggerShow = function (e) {
                e.preventDefault();
                axilKey._navsearch.addClass('visible');
            };

            searchTriggerHide = function (e) {
                e.preventDefault();
                axilKey._navsearch.removeClass('visible');
            };

            axilaccordion = function (e) {
                e.preventDefault();
                $(this).siblings('.collapse.show').parent().removeClass('open').toggleClass('active');
            };
            

            axilKey._document
                .on('blur', 'input, textarea, select', inputblur)
                .on('focus', 'input, input:not([type="radio"]), input:not([type="checkbox"]), textarea,select', inputFocus)
                .on('click', '#side-nav-toggler', openSideNav)
                .on('click', '#close-sidenav , .side-nav-opened', closeSideNav)
                .on('click' , '.search-trigger' , searchTriggerShow)
                .on('click' , '.axil-search-area .navbar-search-close' , searchTriggerHide)
                .on('click' , '.axil-accordion .card .card-header' , axilaccordion)
                .on('click' , '.popup-navigation-activation' , OpenMobileMenu)
                .on('click' , '.close-menu' , closeMobileMenu)
                .on('click' , '.popup-mobile-manu' , closeMenuWrapperClick)
        },

        _disable_load_more_button: function(){
            //Disable load more button for isotope cat
            this._document.on("click", ".messonry-button button", function (e) {
                var _slug = $(this).data("filter"),
                    _has_load_more = $(".load-more").length;
                _slug = _slug.replace(".", "");
                if (_slug != "*" && _has_load_more > 0) {
                    $(".load-more").prop("disabled", true);
                    $(".load-more").addClass("d-none");
                } else {
                    if (_has_load_more > 0) {
                        $(".load-more").removeClass("d-none");
                        $(".load-more").prop("disabled", false);
                    }
                }
            });
        }
    };

    axilKey.k();

})(window, document, jQuery);
