<?php
/**
 * The template for displaying team single
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package keystroke
 */
get_header();
while ( have_posts() ) : the_post();
    $designation = axil_get_acf_data("team_designation");
    $team_social_icons = axil_get_acf_data("axil_team_social_icons");
    ?>
    <!-- Start Team Details Area  -->
    <div class="axil-team-details-area axil-team-details-wrapper theme-gradient-4">
        <div class="container">
            <div class="row align-items-center row--60">
                <div class="col-lg-6 col-xl-5 col-md-12 col-12 order-2 order-lg-1">
                    <div class="team-details-inner">
                        <div class="inner">
                            <h1 class="title"><?php the_title(); ?></h1>
                            <?php if($designation){ ?>
                                <span class="subtitle"><?php echo esc_html($designation); ?></span>
                            <?php } ?>
                            <?php
                            if(class_exists('ACF')){
                                // check if the repeater field has rows of data
                                if( have_rows('axil_team_social_icons') ):
                                    ?> <ul class="social-share d-flex justify-content-start liststyle flex-wrap"><?php
                                    // loop through the rows of data
                                    while ( have_rows('axil_team_social_icons') ) : the_row();
                                        // display a sub field value
                                        ?>
                                        <li><a href="<?php the_sub_field('team_enter_social_icon_link'); ?>"><?php the_sub_field('team_enter_social_icon_markup'); ?></a></li>
                                    <?php

                                    endwhile;
                                    ?></ul><?php
                                else :
                                    // no rows found
                                endif;
                            }
                            ?>
                            <div class="subtitle-2"><?php the_content(); ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xl-6 offset-xl-1 col-md-12 col-12 mt_md--40 mt_sm--40 order-1 order-lg-2 mb_sm--20 mb_md--20">
                    <div class="thumbnail text-right">
                        <div class="inner">
                            <?php the_post_thumbnail('axil-team-thumb', ['class' => 'w-100 paralax-image']); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Team Details Area  -->
    <?php
endwhile; // End of the loop.
get_footer();