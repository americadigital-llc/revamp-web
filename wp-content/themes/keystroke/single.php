<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package keystroke
 */

get_header();
// Get Value
$axil_options = Helper::axil_get_options();
$axil_single_blog_sidebar_class = ($axil_options['axil_single_pos'] === 'full')  || !is_active_sidebar( 'sidebar-1' )? 'col-12':'col-lg-8 col-md-12 col-12';
?>
    <!-- Start Blog Details Area  -->
    <div class="axil-blog-area ax-section-gap bg-color-white">
        <div class="container">
            <div class="row row--40">
                <?php if ( is_active_sidebar( 'sidebar-1' ) && $axil_options['axil_single_pos'] == 'left') { ?>
                    <div class="col-lg-4 col-md-12 col-12 mt_md--40 mt_sm--40">
                        <?php dynamic_sidebar(); ?>
                    </div>
                <?php } ?>
                <div class="<?php echo esc_attr($axil_single_blog_sidebar_class); ?>">
                    <div class="axil-blog-details-area">
                        <?php
                        while ( have_posts() ) :
                            the_post();

                            get_template_part( 'template-parts/content-single', get_post_type() );

                        endwhile; // End of the loop.
                        ?>
                    </div>
                </div>
                <?php if ( is_active_sidebar( 'sidebar-1' ) && $axil_options['axil_single_pos'] == 'right') { ?>
                    <div class="col-lg-4 col-md-12 col-12 mt_md--40 mt_sm--40">
                        <?php dynamic_sidebar(); ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- End Blog Details Area  -->
<?php
get_footer();
